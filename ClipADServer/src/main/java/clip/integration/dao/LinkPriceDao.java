package clip.integration.dao;

import java.util.List;

import clip.integration.bean.LinkPriceItem;

public interface LinkPriceDao {
	
	public List<LinkPriceItem> selectItemList();
	
	public LinkPriceItem selectItem(LinkPriceItem param);
	
}
