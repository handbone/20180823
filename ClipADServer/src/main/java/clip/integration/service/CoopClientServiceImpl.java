package clip.integration.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import clip.common.util.HttpClientUtil;
import clip.common.util.TelegramUtil;
import clip.integration.bean.CoopMessage;
import clip.integration.bean.ext.CoopPointCouponMessage;
import clip.integration.service.cooputil.StringEncrypter;

@Service("coopClientService")
public class CoopClientServiceImpl implements CoopClientService {
	Logger logger = Logger.getLogger(this.getClass());
	
	//쿠프 쿠폰 서버
	@Value("#{props['COOP_COUPON_SERVER_URL']}")
	private String COUPON_SERVER_URL;
	//쿠프 쿠폰 서버 - clip 서버 코드 , 패스워드
	@Value("#{props['COOP_COUPON_COMP_CODE']}")
	private String COMP_CODE;
	@Value("#{props['COOP_COUPON_COMP_PASS']}")
	private String COMP_PASS;

	//쿠프 이미지 서버 URL
	@Value("#{props['COOP_BARCODE_IMG_SERVER']}") private String COOP_BARCODE_IMG_SERVER;
	@Value("#{props['COOP_BARCODE_IMG_PARAM']}") private String COOP_BARCODE_IMG_PARAM;
	@Value("#{props['COOP_BARCODE_IMG_PARAM_S']}") private String COOP_BARCODE_IMG_PARAM_S;
	@Value("#{props['COOP_BARCODE_IMG_PARAM_ENC_KEY']}") private String COOP_BARCODE_IMG_PARAM_ENC_KEY;
	@Value("#{props['COOP_BARCODE_IMG_PARAM_ENC_IV']}") private String COOP_BARCODE_IMG_PARAM_ENC_IV;
	
	//쿠프 포인트 쿠폰
	@Value("#{props['POINT_COUPON_URL']}")
	private String POINT_COUPON_URL;
	@Value("#{props['POINT_COUPON_COMP_CODE']}")
	private String POINT_COUPON_COMP_CODE;
	@Value("#{props['POINT_COUPON_COMP_PASS']}")
	private String POINT_COUPON_COMP_PASS;
	
	@Autowired
	ClipPointClientService clipPointClientService;

	@Override
	public CoopMessage issueCoupon(CoopMessage param) {
		
		CoopMessage result = new CoopMessage();
		
		//더미데이터
		/*result.setRetMsg("성공");
		result.setCouponId("XXXXX");
		result.setPinId("XXXX");
		result.setResult("S");*/
		
		//TODO
		//if(param != null) return result;
		
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("GET", COUPON_SERVER_URL);
			conn.addHeader("Content-Type", "text/html");
			
			conn.addParameter("CODE", COMP_CODE);
			conn.addParameter("PASS", COMP_PASS);
			conn.addParameter("COUPONCODE", param.getGoodId());
			conn.addParameter("SEQNUMBER", param.getTrId());
			//conn.addParameter("CREATECOUNT", "2");
			conn.addParameter("FILLER01", param.getFiller01());
			conn.addParameter("FILLER02", "");
			conn.addParameter("FILLER03", "");
			
			/*CODE	STRING	Client 코드	필수
			PASS	STRING	연동 Password	필수
			COUPONCODE	STRING	상품코드	필수
			SEQNUMBER	STRING	거래번호	필수
			FILLER01	STRING	임시공간(미 사용시 공백 회신)	선택
			FILLER02	STRING	임시공간(미 사용시 공백 회신)	선택
			FILLER03	STRING	임시공간(미 사용시 공백 회신)	선택*/			
			
			logger.info("OfflinePay request : good_id["+param.getGoodId()+"],user_token["+param.getFiller01()+"]");
			
			int ret = conn.sendRequest();
			
			logger.info("OfflinePay response status : " + ret);
			if(HttpStatus.SC_OK == ret) {
				
				result = conn.getResponseXml(new CoopMessage());
				
				logger.debug("COOP_RESULT_CODE : "+ result.getRetCode());
				
				/*
				RESULTCODE	STRING	응답코드
				RESULTMSG	STRING	응답메세지
				COUPONNUMBER	STRING	쿠폰번호
				PINNUMBER	STRING	핀번호(모바일발송코드)
				CODE	STRING	Client 코드
				PASS	STRING	연동 Password
				COUPONCODE	STRING	상품코드
				SEQNUMBER	STRING	거래번호
				FILLER01	STRING	임시공간(미 사용시 공백 회신)
				FILLER02	STRING	임시공간(미 사용시 공백 회신)
				FILLER03	STRING	임시공간(미 사용시 공백 회신)
				*/
				
				if("00".equals(result.getRetCode()) || "71".equals(result.getRetCode())) {
					result.setResult("S");
					logger.info("OfflinePay Success : retcode["+result.getRetCode()+"],pinId["+result.getPinId()+"]");
				} else {
					logger.info("OfflinePay Error Fail Response : retcode["+result.getRetCode()+"] ");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("처리도중 오류가 발생하였습니다.");
				logger.info("OfflinePay Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			result.setResult("F");
			result.setResultMsg("처리도중 오류가 발생하였습니다.");
			logger.info("OfflinePay Error System : message["+e.getMessage()+"] ");
			
			logger.error(e.getMessage());
		}
		
		return result;
	}
	
	/*
	Inumber연동가이드_v3_9_20151204

	2.6 쿠폰발급 – 단일발급 (별도 데이터 저장)
	WebService  URL : /serviceapi.asmx
	API 명 : ServiceCouponCreateFiller
	용도 : 쿠폰 번호 1개 발급용으로 업체 추가 데이터 3개까지 저장

	입력 Parameter

	매개변수명	타입	설명	필수 구분
	CODE	STRING	Client 코드	필수
	PASS	STRING	연동 Password	필수
	COUPONCODE	STRING	상품코드	필수
	SEQNUMBER	STRING	거래번호	필수
	FILLER01	STRING	임시공간(미 사용시 공백 회신)	선택
	FILLER02	STRING	임시공간(미 사용시 공백 회신)	선택
	FILLER02	STRING	임시공간(미 사용시 공백 회신)	선택


	반환 값

	매개변수명	타입	설명
	RESULTCODE	STRING	응답코드
	RESULTMSG	STRING	응답메세지
	COUPONNUMBER	STRING	쿠폰번호
	PINNUMBER	STRING	핀번호(모바일발송코드)
	CODE	STRING	Client 코드
	PASS	STRING	연동 Password
	COUPONCODE	STRING	상품코드
	SEQNUMBER	STRING	거래번호
	FILLER01	STRING	임시공간(미 사용시 공백 회신)
	FILLER02	STRING	임시공간(미 사용시 공백 회신)
	FILLER02	STRING	임시공간(미 사용시 공백 회신)
	*/
	

	@Override
	public CoopMessage getBarcodeUrl(CoopMessage param) {
		CoopMessage result = new CoopMessage();
		
		try {
			
			//TODO param
			if(StringUtils.isNotEmpty(param.getPinId())) {
				StringEncrypter encrypter = new StringEncrypter(COOP_BARCODE_IMG_PARAM_ENC_KEY, COOP_BARCODE_IMG_PARAM_ENC_IV);
				String pId = encrypter.encrypt(param.getPinId());
				result.setBarcodeImgUrl(COOP_BARCODE_IMG_SERVER+COOP_BARCODE_IMG_PARAM_S+pId);
				result.setResult("S");
				
				throw new Exception();
			} else {
				result.setResult("F");
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			result.setResult("F");
			
			//TODO 제거 - 테스트용
			result.setResult("S");
			result.setBarcodeImgUrl("https://image.multicon.co.kr/createimage/?data=LsTJQuUJOMkm46DgTSBxqcUU0kAhxeFBLVyu0wxe4Dw=");
		}
	
		return result;
	}

	@Override
	public CoopPointCouponMessage pointCouponCheck(CoopPointCouponMessage coopPointCouponMessage, HttpServletRequest request) throws Exception {
		CoopPointCouponMessage result = new CoopPointCouponMessage();
		//StringBuffer logbuf = new StringBuffer();

		HttpClientUtil conn = null;
		try {
			logger.debug("INUMBER3_CONNECTION_TRY");
			conn = new HttpClientUtil("GET", POINT_COUPON_URL);
			conn.addHeader("Content-Type", "text/html");
			
			conn.addParameter("PASS", POINT_COUPON_COMP_PASS);
			conn.addParameter("COUPON_TYPE", "00");
			conn.addParameter("COMMAND", "L1");
			conn.addParameter("COMP_CODE", POINT_COUPON_COMP_CODE);
			conn.addParameter("COUPON_NUMBER", coopPointCouponMessage.getCouponNo());
			conn.addParameter("BRANCH_CODE", "");
			conn.addParameter("USE_PRICE", "");
			conn.addParameter("CancelAuthCode", "");
			
			String tr_id = TelegramUtil.createSimpleTrId();
			conn.addParameter("L3_SEQNUMBER", tr_id);
			/*
			PASS	STRING	사용자암호
			COUPON_TYPE	STRING	쿠폰 종류 코드
					공백 or 00 : 상품 교환권
					01 : 할인권
					02 : 디지털 금액권 (잔액관리형)
			COMMAND	STRING	처리요청 코드
					L0 : 쿠폰 조회
					L1 : 쿠폰 인증
					L2 : 쿠폰 인증 취소
					L3 : 망거래 취소
			COMP_CODE	STRING	제휴사 코드
			COUPON_NUMBER	STRING	쿠폰 번호
			BRANCH_CODE	STRING	매장 코드
			USE_PRICE	STRING	디지털 금액권인 경우 사용 금액, 그 외는 공백
			CancelAuthCode	STRING	디지털 금액권인 경우 취소시 원승인 번호, 그 외는 공백
			L3_SEQNUMBER	STRING	망취소용 거래번호
			*/
			logger.info("PointCoupon Check request : coupon_no["+coopPointCouponMessage.getCouponNo()+"]");
			
			int ret = conn.sendRequest();
			logger.info("PointCoupon Check response status : " + ret);
            
			if(HttpStatus.SC_OK == ret) {
				result = conn.getResponseXml(new CoopPointCouponMessage());
				
				// [jaeheung] 2017.08.07 쿠프 연결 성공 시 등록완료와 나머지 결과로 분기
				if(CoopPointCouponMessage.RET_SUCCESS.equals(result.getResultcode()) == false) {
					result.setResult("FF");
					logger.info("PointCoupon Check Error Fail Response : retcode["+result.getResultcode()+"] ");
				} else {
					result.setResult("S");
					
					logger.info("PointCoupon Check Success : retcode["+result.getResultcode()+"]");
				}				
				// [jaeheung] 2017.08.07 쿠폰 등록 결과 추가
				result.setResultcode(result.getResultcode());
				result.setResultMsg(makePointCouponResultMsg(result.getResultcode()));
				
				/*
				RESULTCODE	STRING	응답코드
				RESULTMSG	STRING	응답메세지
				COMMAND		STRING	처리 코드
				COUPON_NAME	STRING	쿠폰 명칭
				USE_PRICE	STRING	소비자 가격
				PRODUCT_CODE	STRING	상품 코드
				BAL_PRICE	STRING	사용 가능한 금액 (잔액)
				AUTH_COUNT	STRING	사용 가능 건수
				COUPON_TYPE	STRING	쿠폰 종류 코드
							공백 or 00 : 상품 교환권
							01 : 할인권
							02 : 디지털 금액권 (잔액관리형)
				USE_YN		STRING	사용 여부
				BRANCE_CODE	STRING	매장코드 (사용된 쿠폰일 경우 사용된 매장코드 반환)
				AUTH_CODE	STRING	승인 번호
							( 디지털 금액권 외 쿠폰에 대해서는 협의/요청된 브랜드만 회신 )
				USE_DATE	STRING	사용일시 ( 사용된 쿠폰일 경우 회신 )
				SEL_PRICE	STRING	판매가격 ( 협의된 브랜드만 회신 )
				GW_CODE		STRING	카카오 브랜드 ID ( 협의된 브랜드만 회신 )
				START_DAY	STRING	유효기간 시작일 ( Format : yyyy-MM-dd )
				END_DAY		STRING	유효기간 종료일 ( Format : yyyy-MM-dd )
				*/
				
			} else {
				result.setResult("F");
				result.setResultcode("91");
				
				// [jaeheung] 2017.08.07 쿠폰등록 연동 실패 결과 메시지 추가
				if( StringUtils.isNotEmpty(result.getResultmsg()) ) {
					result.setResultMsg(result.getResultmsg());
					logger.info("PointCoupon Check Error Fail Response : retcode["+result.getResultcode()+"] ");
				} else {
					result.setResultMsg("API 연동도중 오류가 발생하였습니다.");
					logger.info("PointCoupon Check Error Fail Response");
				}
				
				logger.info("PointCoupon Check Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			
			result.setResult("F");
			// [jaeheung] 2017.08.07 쿠폰등록 연동 Exception 결과 추가
			result.setResultcode("91");
			if( StringUtils.isNotEmpty(e.getMessage()) ) {
				result.setResultMsg(e.getMessage());
			} else {
				result.setResultMsg("API 연동도중 Exception 오류가 발생하였습니다.");
			}
			
			logger.info("PointCoupon Check Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
	}
	
	public static final String RET_SUCCESS = "00";
	//거래번호 중복요청 오류
	public static final String RET_NOT_ESXIST_ERR = "01";	//01	미존재쿠폰
	public static final String RET_IVALID_COUPON_ERR = "02";		//02	비정상쿠폰번호
	public static final String RET_PERIOD_END_ERR = "04";	//04	기간만료쿠폰
	public static final String RET_PAYED_ERR = "05";		//05	사용된 쿠폰
	public static final String RET_CANCEL_ERR = "06";		//06	결제취소 쿠폰

	public static final String RET_PRODUCT_CODE_ERR_1 = "80";	//80 상품 종류 코드 불일치 오류
	public static final String RET_PRODUCT_CODE_ERR_2 = "81";	//81 상품 종류 코드 불일치 오류
	public static final String RET_PRODUCT_CODE_ERR_3 = "84";	//84 상품 종류 코드 불일치 오류	

	private static String makePointCouponResultMsg(String code){
		String result = "";
		switch(code) {
			case CoopPointCouponMessage.RET_SUCCESS :
				result = "정상적으로 처리되었습니다.";
				break;
			case CoopPointCouponMessage.RET_NOT_ESXIST_ERR : 
				result = "없는 쿠폰 번호입니다.";
				break;
			case CoopPointCouponMessage.RET_IVALID_COUPON_ERR : 
				result = "비정상 쿠폰 번호입니다.";
				break;
			case CoopPointCouponMessage.RET_PERIOD_END_ERR : 
				result = "기간이 만료된 쿠폰입니다.";
				break;
			case CoopPointCouponMessage.RET_PAYED_ERR : 
				result = "이미 사용된 쿠폰입니다.";
				break;
			case CoopPointCouponMessage.RET_CANCEL_ERR : 
				result = "결제 취소된 쿠폰입니다.";
				break;
			case CoopPointCouponMessage.RET_PRODUCT_CODE_ERR_1 : 
				result = "상품 종류에 맞지 않는 쿠폰입니다.";
				break;
			case CoopPointCouponMessage.RET_PRODUCT_CODE_ERR_2 : 
				result = "상품 종류에 맞지 않는 쿠폰입니다.";
				break;
			case CoopPointCouponMessage.RET_PRODUCT_CODE_ERR_3 : 
				result = "상품 종류에 맞지 않는 쿠폰입니다.";
				break;
			default :
				result = "정상적으로 처리되지 않았습니다. code : " + code;
				break;
		}
		
		return result;
	}

	@Override
	public CoopPointCouponMessage pointCouponCancel(CoopPointCouponMessage orgMsg) throws Exception {
		CoopPointCouponMessage result = new CoopPointCouponMessage();
		
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("GET", POINT_COUPON_URL);
			conn.addHeader("Content-Type", "text/html");
			//http://v3authtest.m2i.kr:9999/dll/AuthWebService.asmx/Inumber3CouponAuth?
			//PASS=g9PJGmeh6BaSfprJx1xkAQ&COUPON_TYPE=00&COMMAND=L0&COMP_CODE=CP00&COUPON_NUMBER= 004133576960&BRANCH_CODE=1234567890&USE_PRICE= &CancelAuthCode=&L3_SEQNUMBER=
			conn.addParameter("PASS", POINT_COUPON_COMP_PASS);
			conn.addParameter("COUPON_TYPE", "00");
			conn.addParameter("COMMAND", "L3");
			conn.addParameter("COMP_CODE", POINT_COUPON_COMP_CODE);
			conn.addParameter("COUPON_NUMBER", orgMsg.getCouponNo());
			conn.addParameter("BRANCH_CODE", "");
			conn.addParameter("USE_PRICE", "");
			conn.addParameter("CancelAuthCode", "");
			conn.addParameter("L3_SEQNUMBER", orgMsg.getTr_id());
			/*
			PASS	STRING	사용자암호
			COUPON_TYPE	STRING	쿠폰 종류 코드
					공백 or 00 : 상품 교환권
					01 : 할인권
					02 : 디지털 금액권 (잔액관리형)
			COMMAND	STRING	처리요청 코드
					L0 : 쿠폰 조회
					L1 : 쿠폰 인증
					L2 : 쿠폰 인증 취소
					L3 : 망거래 취소
			COMP_CODE	STRING	제휴사 코드
			COUPON_NUMBER	STRING	쿠폰 번호
			BRANCH_CODE	STRING	매장 코드
			USE_PRICE	STRING	디지털 금액권인 경우 사용 금액, 그 외는 공백
			CancelAuthCode	STRING	디지털 금액권인 경우 취소시 원승인 번호, 그 외는 공백
			L3_SEQNUMBER	STRING	망취소용 거래번호
			*/
			
			int ret = conn.sendRequest();

			if(HttpStatus.SC_OK == ret) {
				result = conn.getResponseXml(new CoopPointCouponMessage());
				
				logger.debug("RESULT_CODE : " + result.getResultcode());
				result.setResultMsg(makePointCouponResultMsg(result.getResultcode()));
				if(CoopPointCouponMessage.RET_SUCCESS.equals(result.getResultcode()) == false) {
					result.setResult("F");
					logger.info("PointCoupon Cancel Error Fail Response : retcode["+result.getResultcode()+"]");
				} else {
					result.setResult("S");
					logger.info("PointCoupon Cancel Success : retcode["+result.getResultcode()+"]");
				}
			} else {
				result.setResult("F");
				
				// [jaeheung] 2017.08.07 망거래 취소도중 연동 오류 발생시 결과값 추가
				result.setResultcode("91");
				if(StringUtils.isNotEmpty(result.getResultmsg())) {
					result.setResultMsg(result.getResultmsg());
					logger.info("PointCoupon Cancel Error Fail : retcode["+result.getResultcode()+"]");
				} else {
					result.setResultMsg("망거래 취소도중 연동 오류가 발생하였습니다.");
					logger.info("PointCoupon Cancel Error Fail Response");
				}
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			logger.error(e.getMessage());
			result.setResult("F");
			// [jaeheung] 2017.08.07 망거래 취소도중 Exception 발생시 결과값 추가
			result.setResultcode("91");
			result.setResultMsg("망거래 취소도중 Exception 오류가 발생하였습니다.");
			logger.info("PointCoupon Cancel Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
	}

}