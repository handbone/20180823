package clip.integration.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import clip.common.util.DateUtil;
import clip.common.util.HttpClientUtil;
import clip.integration.bean.LinkPriceItem;
import clip.integration.bean.LinkPriceReportItem;
import clip.integration.bean.LinkPriceReportMessage;
import clip.integration.bean.LinkPriceTransItem;
import clip.integration.bean.LinkPriceTransMessage;
import clip.integration.dao.LinkPriceDao;

@Service
public class LinkPriceClientServiceImpl implements LinkPriceClientService {
	
	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['LINKPRICE_BASE_URL']}") private String LINKPRICE_BASE_URL;
	@Value("#{props['LINKPRICE_USER']}") private String LINKPRICE_USER;
	@Value("#{props['LINKPRICE_PASS']}") private String LINKPRICE_PASS;
	
	private static final String RESULT_CODE_SUCCESS = "0000";
	
	@Autowired
	private LinkPriceDao linkPriceDao;
	
	@Override
	public LinkPriceReportMessage getNextSaving(LinkPriceReportMessage param) {
		/*//파라미터 체크
		//* yyyymm
		//* uId

		//더미데이타
		param.setResult("1");
		param.setResultCode("0000");
		param.setResultMsg("정상 처리되었습니다.");
		param.setOrderSum("10000");
		
		//TODO --- 아래부터가 진짜임
		if(param != null)
			return param;*/
		
		//TODO --- 아래부터가 진짜임
		HttpClientUtil conn = null;
		LinkPriceReportMessage result = new LinkPriceReportMessage();
				
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_sum&yyyymm=201705&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_sum");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(-1), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());
			
			logger.info("Shopbuy NextSaving request : u_id["+param.getuId()+"]");
			int ret = conn.sendRequest();
			
			logger.info("Shopbuy NextSaving response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessage.class);
				
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());
				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					result.setResult("S");
					logger.debug("Order sum : "+result.getOrder_sum());
					logger.info("Shopbuy NextSaving Success : retcode["+result.getResultCode()+"],order_sum["+result.getOrder_sum()+"]");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("Shopbuy NextSaving Error Fail Response : retcode["+result.getResultCode()+"] ");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("오류가 발생하였습니다.");
				logger.info("Shopbuy NextSaving Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			conn.closeConnection();
			logger.info("Shopbuy NextSaving Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
	}

	@Override
	public LinkPriceReportMessage getNextSavingHistory(LinkPriceReportMessage param) {
		/*//파라미터 체크
		//* yyyymm
		//* uId
		
		//더미데이타
		param.setResult("1");
		param.setResultCode("0000");
		param.setResultMsg("정상 처리되었습니다.");
		
		List<LinkPriceReportItem> itemList = new ArrayList<LinkPriceReportItem>();
		param.setReportItemList(itemList);
		
		for(int i=20; i>0; i--) {
			LinkPriceReportItem item = new LinkPriceReportItem();
			item.setMerchantName("G마켓");
			item.setYyyymmdd("201705" + StringUtils.leftPad((i+1)+"", 2, "0") );
			item.setHhmiss(StringUtils.left(i+"", 2) + "0000");
			item.setSales((i * 10000) + "");
			item.setCommission((i * 10) + "");
			itemList.add(item);
		}
		
		
		//TODO --- 아래부터가 진짜임
		if(param != null)
			return param;*/
		
		
		HttpClientUtil conn = null;
		LinkPriceReportMessage result = new LinkPriceReportMessage();
		
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_list&yyyymm=201705&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_list");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(-1), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());

			logger.info("Shopbuy NextSavingHistory request : u_id["+param.getuId()+"]");
			int ret = conn.sendRequest();
			
			logger.info("Shopbuy NextSavingHistory response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessage.class);
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());
				
				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					
					List<LinkPriceReportItem> newList = new ArrayList<LinkPriceReportItem>();
					for(LinkPriceReportItem data : result.getOrder_list()) {
						
						//TODO 스테이터스 체크
						
						//newList.add(data);
					}
					
					result.setResult("S");
					logger.info("Shopbuy NextSavingHistory Success : retcode["+result.getResultCode()+"],list_count["+(newList == null?0:newList.size())+"]");
					
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("Shopbuy NextSavingHistory Error Fail Response : retcode["+result.getResultCode()+"] ");
				}
			} else {
				result.setResult("F");
				logger.info("Shopbuy NextSavingHistory Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			
			conn.closeConnection();
			logger.info("Shopbuy NextSavingHistory Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
	}

	@Override
	public LinkPriceReportMessage getNext2Saving(LinkPriceReportMessage param) {
		/*//파라미터 체크
		//* yyyymm
		//* uId

		//더미데이타
		param.setResult("1");
		param.setResultCode("0000");
		param.setResultMsg("정상 처리되었습니다.");
		param.setOrderSum("20000");
		
		//TODO --- 아래부터가 진짜임
		if(param != null)
			return param;*/
		
		HttpClientUtil conn = null;
		LinkPriceReportMessage result = new LinkPriceReportMessage();
		
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_sum&yyyymm=201706&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_sum");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(0), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());

			logger.info("Shopbuy Next2Saving request : u_id["+param.getuId()+"]");
			int ret = conn.sendRequest();
			
			logger.info("Shopbuy Next2Saving response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessage.class);
				
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());
				
				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					result.setResult("S");
					logger.info("Shopbuy Next2Saving Success : retcode["+result.getResultCode()+"],order_sum["+result.getOrder_sum()+"]");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("Shopbuy Next2Saving Error Fail Response : retcode["+result.getResultCode()+"] ");
				}
				
				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					result.setResult("S");
				}
				
			} else {
				result.setResult("F");
				logger.info("Shopbuy Next2Saving Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			
			conn.closeConnection();
			logger.info("Shopbuy Next2Saving Error System : message["+e.getMessage()+"] ");
		}

		
		
		return result;
	}

	@Override
	public LinkPriceReportMessage getNext2SavingHistory(LinkPriceReportMessage param) {
		/*//파라미터 체크
		//* yyyymm
		//* uId

		//더미데이타
		param.setResult("1");
		param.setResultCode("0000");
		param.setResultMsg("정상 처리되었습니다.");
		
		List<LinkPriceReportItem> itemList = new ArrayList<LinkPriceReportItem>();
		param.setReportItemList(itemList);
		
		for(int i=20; i>0; i--) {
			LinkPriceReportItem item = new LinkPriceReportItem();
			item.setMerchantName("G마켓");
			item.setYyyymmdd("201704" + StringUtils.leftPad((i+1)+"", 2, "0"));
			item.setHhmiss(StringUtils.left(i+"", 2) + "0000");
			item.setSales((i * 10000) + "");
			item.setCommission((i * 10) + "");
			itemList.add(item);
		}
		
		
		//TODO --- 아래부터가 진짜임
		if(param != null)
			return param;*/
		
		//TODO --- 아래부터가 진짜임
		HttpClientUtil conn = null;
		LinkPriceReportMessage result = new LinkPriceReportMessage();
		
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_list&yyyymm=201706&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_list");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(0), "yyyyMM"));
			conn.addParameter("u_id", param.getuId());
			
			logger.info("Shopbuy Next2SavingHistory request : u_id["+param.getuId()+"]");
			int ret = conn.sendRequest();
			
			logger.info("Shopbuy Next2SavingHistory response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessage.class);
				
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());

				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					List<LinkPriceReportItem> newList = new ArrayList<LinkPriceReportItem>();
					for(LinkPriceReportItem data : result.getOrder_list()) {
						
						//TODO 스테이터스 체크
						
						//newList.add(data);
					}
					
					result.setResult("S");
					logger.info("Shopbuy Next2SavingHistory Success : retcode["+result.getResultCode()+"],list_count["+(newList == null?0:newList.size())+"]");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("Shopbuy Next2SavingHistory Error Fail Response : retcode["+result.getResultCode()+"] ");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("오류가 발생하였습니다.");
				logger.info("Shopbuy Next2SavingHistory Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			
			conn.closeConnection();
			logger.info("Shopbuy Next2SavingHistory Error System : message["+e.getMessage()+"] ");
		}

		return result;
	}

	@Override
	public LinkPriceTransMessage getTransSaving(LinkPriceTransMessage param) {
		//파라미터 체크
		//* yyyymmdd
		
		//더미데이타
		param.setResult("0");
		
		List<LinkPriceTransItem> itemList = new ArrayList<LinkPriceTransItem>();
		param.setTransItemList(itemList);
		
		for(int i=0; i<20; i++) {
			LinkPriceTransItem item = new LinkPriceTransItem();
			//TODO 구입처(쇼핑몰)
			item.setmId("G마켓");
			item.setYyyymmdd("201706" + StringUtils.leftPad(i+"", 2));
			item.setHhmiss(StringUtils.left(i+"", 2) + "0000");
			item.setSales((i * 10000) + "");
			item.setCommission((i * 10) + "");
			//TODO 상태정의 - 적립/적립취소 구분
			item.setStatus("100");
			itemList.add(item);
		}

		return param;
	}

	@Override
	public List<LinkPriceItem> getItemList(LinkPriceItem param) {
		return linkPriceDao.selectItemList();
	}

	@Override
	public LinkPriceItem getItemInfo(LinkPriceItem param) {
		
		LinkPriceItem  ret = linkPriceDao.selectItem(param);

		return ret;
	}

	@Override
	public LinkPriceReportMessage getNowSaving(LinkPriceReportMessage params) {
		/*//파라미터 체크
		//* yyyymm
		//* uId

		//더미데이타
		param.setResult("1");
		param.setResultCode("0000");
		param.setResultMsg("정상 처리되었습니다.");
		
		List<LinkPriceReportItem> itemList = new ArrayList<LinkPriceReportItem>();
		param.setReportItemList(itemList);
		
		for(int i=20; i>0; i--) {
			LinkPriceReportItem item = new LinkPriceReportItem();
			item.setMerchantName("G마켓");
			item.setYyyymmdd("201704" + StringUtils.leftPad((i+1)+"", 2, "0"));
			item.setHhmiss(StringUtils.left(i+"", 2) + "0000");
			item.setSales((i * 10000) + "");
			item.setCommission((i * 10) + "");
			itemList.add(item);
		}
		
		
		//TODO --- 아래부터가 진짜임
		if(param != null)
			return param;*/
		
		//TODO --- 아래부터가 진짜임
		HttpClientUtil conn = null;
		LinkPriceReportMessage result = new LinkPriceReportMessage();
		
		try {
			//http://api.linkprice.com/affiliate/openapi.php?user=A100000131&auth_key=bcb2842d247fdf4fb9b5c7568d78883e&method=report.kt_clip_list&yyyymm=201706&u_id=test1234
			conn = new HttpClientUtil("GET", LINKPRICE_BASE_URL);
			conn.addHeader("Content-type", "text/html");
			
			conn.addParameter("method", "report.kt_clip_list");
			conn.addParameter("user", LINKPRICE_USER);
			conn.addParameter("auth_key", LINKPRICE_PASS);
			conn.addParameter("yyyymm", DateUtil.getDate2String(DateUtil.addMonth(-2), "yyyyMM"));
			conn.addParameter("u_id", params.getuId());
			
			logger.info("Shopbuy Next2SavingHistory request : u_id["+params.getuId()+"]");
			int ret = conn.sendRequest();
			
			logger.info("Shopbuy Next2SavingHistory response status : " + ret);
			
			if(ret == HttpStatus.SC_OK) {
				result = conn.getResponseJson(LinkPriceReportMessage.class);
				
				logger.debug("Res : "+result.getResultCode()+", "+result.getResultMsg());

				if(RESULT_CODE_SUCCESS.equals(result.getResultCode())) {
					List<LinkPriceReportItem> newList = new ArrayList<LinkPriceReportItem>();
					for(LinkPriceReportItem data : result.getOrder_list()) {
						
						//TODO 스테이터스 체크
						logger.info(data.toString());
					}
					
					result.setResult("S");
					logger.info("Shopbuy Next2SavingHistory Success : retcode["+result.getResultCode()+"],list_count["+(newList == null?0:newList.size())+"]");
				} else {
					result.setResult("F");
					result.setResultMsg(result.getResultMsg());
					logger.info("Shopbuy Next2SavingHistory Error Fail Response : retcode["+result.getResultCode()+"] ");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("오류가 발생하였습니다.");
				logger.info("Shopbuy Next2SavingHistory Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			result.setResult("F");
			result.setResultMsg("오류가 발생하였습니다.");
			
			conn.closeConnection();
			logger.info("Shopbuy Next2SavingHistory Error System : message["+e.getMessage()+"] ");
		}

		return result;
	}
	
	

}
