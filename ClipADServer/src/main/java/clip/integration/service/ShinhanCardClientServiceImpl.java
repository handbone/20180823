package clip.integration.service;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.coopnc.cardpoint.CardPointSwapManager;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointHandler_SHINHAN;
import com.coopnc.cardpoint.util.MessageUtil;

import clip.integration.bean.ShinhanCardMessage;
import clip.pointswap.dao.PointSwapDao;

@Service("shinhanCardClientService")
public class ShinhanCardClientServiceImpl implements ShinhanCardClientService {

	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['POINT_SWAP_INI_FILE']}") private String pointSwapIniFile;
	
	@Value("#{props['API_DOMAIN']}") private String API_DOMAIN;
	
	private Boolean fakeUserCi;

	@Autowired
	private PointSwapDao pointSwapDao;
	
	@PostConstruct
	public void init() {
		if( API_DOMAIN !=null && API_DOMAIN.contains("127.0.0.1") ) {
			fakeUserCi =true;
		}else {
			fakeUserCi =false;
		}
		logger.info("shinhan Card fakecheck " + String.valueOf(fakeUserCi));
	}
	
	@Override
	@Async
	public Future<ShinhanCardMessage> getPoint(ShinhanCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
			}
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo("shinhan");
			String trId = manager.makeDefaultTrId("shinhan", "", ""+ ret, "1");
			
			logger.debug(" generated trId : "+trId);

			param.setReg_source("shinhan");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_SHINHAN(param)).getPoint();
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			if(!"S".equals(param.getResult())) {
				if(StringUtils.isEmpty(param.getRes_code())) {
					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
				}
			}
			
			return new AsyncResult<ShinhanCardMessage>(param);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<ShinhanCardMessage> minusPoint(ShinhanCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
			}
			if(param.isPointOver() || param.getReq_point() == 0){
				// 차감 한계 포인트가 넘었거나 차감포인트를 입력하지 않은 사유 시 02로 리턴
				param.setResult("F");
				param.setRes_code("02");
				return new AsyncResult<ShinhanCardMessage>(param);
			}
			
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());

			int ret = pointSwapDao.selectDailySerialNo("shinhan");
			String trId = manager.makeDefaultTrId("shinhan", "", ""+ ret, "3");
			
			param.setReg_source("shinhan");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_SHINHAN(param)).usePoint();
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			pointSwapDao.insertPointSwapHist(param);

			if(!"S".equals(param.getResult())) {
				//응답 코드가 없다면 내부 오류나 응답 지연으로 판단해 취소를 보낸다.
				if(StringUtils.isEmpty(param.getRes_code())) {
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(param);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert shinhan old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
//					ShinhanCardMessage cancelMsg = new ShinhanCardMessage();
//					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
//					
//					ret = pointSwapDao.selectDailySerialNo("shinhan");
//					trId = manager.makeDefaultTrId("shinhan", "", ""+ ret, "3");
//
//					cancelMsg.setReg_source("shinhan");
//					cancelMsg.setUser_ci(param.getUser_ci());
//					cancelMsg.setCust_id_org(param.getCust_id_org());
//					cancelMsg.setRef_trans_id(param.getTrans_id());
//					cancelMsg.setTrans_id(trId);
//					cancelMsg.setReq_point(param.getReq_point());
//					
//					cancelMsg.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					manager.setHandler(new PointHandler_SHINHAN(cancelMsg)).netCancelUse();
//					cancelMsg.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					
//					logger.info("PointSwap Cancel Response shinhan result["+cancelMsg.getResult()+"],resultCode["+cancelMsg.getRes_code()+"], avail point["+cancelMsg.getAvail_point() +"], use point["+cancelMsg.getRes_point()+"]");
//					pointSwapDao.insertPointSwapHist(cancelMsg);
				}
			}

			return new AsyncResult<ShinhanCardMessage>(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<ShinhanCardMessage> cancelMinusPoint(ShinhanCardMessage param) {
		try {
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			if(fakeUserCi) {
				param.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
			}
			int ret = pointSwapDao.selectDailySerialNo("shinhan");
			String trId = manager.makeDefaultTrId("shinhan", "", ""+ ret, "3");

			param.setReg_source("shinhan");
			param.setRef_trans_id(param.getTrans_id());
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));

			manager.setHandler(new PointHandler_SHINHAN(param)).netCancelUse();

			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			logger.info("PointSwap Cancel Response shinhan result["+param.getResult()+"],resultCode["+param.getRes_code()+"], avail point["+param.getAvail_point() +"], use point["+param.getRes_point()+"]");
			pointSwapDao.insertPointSwapHist(param);
			
			return new AsyncResult<ShinhanCardMessage>(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<ShinhanCardMessage> uploadBalanceAccounts(ShinhanCardMessage param) {
		// TODO Auto-generated method stub
		param.setResult("F");
		return new AsyncResult<ShinhanCardMessage>(param);
	}

	@Override
	@Async
	public Future<ShinhanCardMessage> shinhanFanCheck(ShinhanCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("1PPAK7sm+csa+THDb9uzuRJ5bj5bRjtw1TBAnP4JmkPi5wwVBH9aJ75lqLkFzbtPcY20DzFq2Vt4IFkIHB/KZQ==");
			}
			logger.debug("start shinhan fan check");

			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo("shinhan");
			String trId = manager.makeDefaultTrId("shinhan", "", ""+ ret, "1");
			
			logger.debug(" generated trId : "+trId);

			param.setReg_source("shinhan");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_SHINHAN(param)).checkFanUser();
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));

			logger.debug(" check result : "+param.getResult());
			
			return new AsyncResult<ShinhanCardMessage>(param);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getSwapConfigFile() {
		try {
			String result = new ClassPathResource(pointSwapIniFile).getURI().getPath();
			return result;
		} catch (IOException e) {
			return "";
		}
	}
}
