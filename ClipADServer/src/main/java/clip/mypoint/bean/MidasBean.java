package clip.mypoint.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MidasBean {
	
	private String cid		;	//화면 ID, 화면ID 쉬트에서 정의함
	private String pid		;	//이전 화면 ID, 화면ID 쉬트에서 정의함
	private String date		;	//수집일시(yyyyMMdd24HHmmss)
	private String custid	;	//사용자 ID(암호화 처리)
	private String os		;	//"단말기 OS 유형 - ANDROID - IOS"
	private String channelid;	//채널 ID (CLiP), MIDAS에서 발급한 ID
	private String appver	;	//앱 버전
	private String tel		;	//"통신사 유형 - KT - SKT - LGT"
	private String gps 		;	//GPS(암호화 처리)
	private String cellid	;	//GSM CELL ID(암호화 처리)
	private String gaid		;	//AAID/IDFA(암호화 처리)
	private String wimac	;		//WIFI MAC ADDRESS
	private String poncst	;	//(단말) 사용자 정의 필드
	private String action	;	//"실행 채널 유형 - C (직접실행) - P (Push) - W (web)"
	private String brandid	;	//브랜드 ID, cid=BEN002, BEN003, BEN004 이면 brandid 추가
	private String cardid	;	//맴버쉽 카드 ID, cid=MCD002, MCD006, MCD022, MCD042, MCD032, PCD002, BEN012 이면 cardid 추가
	private String lftid	;		//전단 ID, cid='NTI012' 이면 lftid 추가
	private String evtid	;		//이벤트 ID, cid='EVT002' 이면 evtid 추가
	private String compid	;	//제휴 ID
	private String cpnid	;		//쿠폰 ID, cid=CPN002, CPN003 이면 cpnid 추가
	private String ext		;	//확장 컬럼
	
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCustid() {
		return custid;
	}
	public void setCustid(String custid) {
		this.custid = custid;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getChannelid() {
		return channelid;
	}
	public void setChannelid(String channelid) {
		this.channelid = channelid;
	}
	public String getAppver() {
		return appver;
	}
	public void setAppver(String appver) {
		this.appver = appver;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getGps() {
		return gps;
	}
	public void setGps(String gps) {
		this.gps = gps;
	}
	public String getCellid() {
		return cellid;
	}
	public void setCellid(String cellid) {
		this.cellid = cellid;
	}
	public String getGaid() {
		return gaid;
	}
	public void setGaid(String gaid) {
		this.gaid = gaid;
	}
	public String getWimac() {
		return wimac;
	}
	public void setWimac(String wimac) {
		this.wimac = wimac;
	}
	public String getPoncst() {
		return poncst;
	}
	public void setPoncst(String poncst) {
		this.poncst = poncst;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getBrandid() {
		return brandid;
	}
	public void setBrandid(String brandid) {
		this.brandid = brandid;
	}
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public String getLftid() {
		return lftid;
	}
	public void setLftid(String lftid) {
		this.lftid = lftid;
	}
	public String getEvtid() {
		return evtid;
	}
	public void setEvtid(String evtid) {
		this.evtid = evtid;
	}
	public String getCompid() {
		return compid;
	}
	public void setCompid(String compid) {
		this.compid = compid;
	}
	public String getCpnid() {
		return cpnid;
	}
	public void setCpnid(String cpnid) {
		this.cpnid = cpnid;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	
	
}
