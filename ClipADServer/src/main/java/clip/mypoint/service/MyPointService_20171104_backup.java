///*[CLiP Point] version [v1.0]
//* Copyright © 2016 kt corp. All rights reserved.
//* This is a proprietary software of kt corp, and you may not use this file except in
//* compliance with license agreement with kt corp. Any redistribution or use of this
//* software, with or without modification shall be strictly prohibited without prior written
//* approval of kt corp, and the copyright notice above does not evidence any actual or
//* intended publication of such software.
//*/
//package clip.mypoint.service;
//
//import clip.mypoint.bean.BannerInfoBean;
//import clip.mypoint.bean.MyPointBean;
//
//public interface MyPointService_20171104_backup {
//
//	MyPointBean getPoint(MyPointBean myPointBean) throws java.lang.Exception;
//
//	MyPointBean getUserCi(MyPointBean myPointBean) throws java.lang.Exception;
//	
//	MyPointBean getPointHistory(MyPointBean myPointBean) throws java.lang.Exception;
//
//	MyPointBean getPointHistoryNew(MyPointBean myPointBean) throws java.lang.Exception;
//
//	BannerInfoBean getBannerInfo(BannerInfoBean bannerInfoBean) throws java.lang.Exception;
//	
//	/************************
//	 **  이하 고도화
//	 ************************/
//	/**
//	 * user_token 조회
//	 * @param userCi
//	 * @return
//	 * @throws java.lang.Exception
//	 */
//	String getUserToken(String userCi) throws java.lang.Exception;
//	
//}
