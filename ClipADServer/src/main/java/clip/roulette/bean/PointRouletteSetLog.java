/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.bean;

public class PointRouletteSetLog {
	 private String idx;
	 private String roulette_id; 
	 private String settime;
	 private String setnum;
	 private String ipaddr;
	 private String regdate;
	 private String sdate;
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getRoulette_id() {
		return roulette_id;
	}
	public void setRoulette_id(String roulette_id) {
		this.roulette_id = roulette_id;
	}
	public String getSettime() {
		return settime;
	}
	public void setSettime(String settime) {
		this.settime = settime;
	}
	public String getSetnum() {
		return setnum;
	}
	public void setSetnum(String setnum) {
		this.setnum = setnum;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	 
	 
	 
	 
}
