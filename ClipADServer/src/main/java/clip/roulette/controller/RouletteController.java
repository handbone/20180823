/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import clip.common.crypto.AES256CipherClip;
import clip.roulette.bean.PointRoulette;
import clip.roulette.bean.PointRouletteJoin;
import clip.roulette.bean.RouletteDto;
import clip.roulette.service.RouletteService;

@Controller
public class RouletteController {
	Logger logger = Logger.getLogger(RouletteController.class.getName());

	@Autowired
	private RouletteService rouletteService;

	/*
	 * 암호화 테스트 
	 */
	@RequestMapping(value ="/crypto.do")
	@ResponseBody
	public ResponseEntity<String> crypto(@RequestParam(required = true) String stringData,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
		logger.debug("makeMediaStat Page start!!!");
		
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity<String>(stringData+" : "+new String(AES256CipherClip.AES_Encode(stringData).getBytes()),responseHeaders, HttpStatus.CREATED);
	}
	/*
	 * 암호화 테스트 
	 */
	@RequestMapping(value ="/decrypto.do") 
	@ResponseBody
	public ResponseEntity<String> decrypto(@RequestParam(required = true) String stringData,HttpServletRequest request, ModelMap model)  throws java.lang.Exception {
		logger.debug("makeMediaStat Page start!!!");
		
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");
		return new ResponseEntity<String>(stringData+" : "+new String(AES256CipherClip.AES_Decode(stringData).getBytes()),responseHeaders, HttpStatus.CREATED);
	}
	
	
	
	/*
	 * 룰렛 메인페이지
	 */
	@RequestMapping(value = "/roulette/rouletteMain.do")
	public ModelAndView rouletteMain(
			@RequestParam(required = true) String roulette_id,
			@RequestParam(required = true) String keyid, 
			@RequestParam(required = true) String keytype,
			@RequestParam(required = false) String returnURL, 
			RouletteDto temprouletteDto,
			PointRouletteJoin temppointRouletteJoin, 
			HttpServletRequest request
			) throws java.lang.Exception {
		logger.debug("rouletteMain Page start!!!");
		
		logger.debug("Parameter ====================");
		logger.debug("roulette_id : "+roulette_id);
		logger.debug("keyid : "+keyid);
		logger.debug("keytype : "+keytype);
		logger.debug("returnURL : "+returnURL);

		if("".equals(keyid) || null == keyid){
			logger.debug("CLIPPOINTERROR 100:'keyid' parameter nothing");
			throw new java.lang.Exception();
		}
		if("".equals(roulette_id) || null == roulette_id){
			logger.debug("CLIPPOINTERROR 101:'roulette_id' parameter nothing");
			throw new java.lang.Exception();
		}
		
		
		RouletteDto rouletteDto = temprouletteDto;
		PointRouletteJoin pointRouletteJoin = temppointRouletteJoin;
		
		// id선택 및 입력
		rouletteDto = rouletteService.selectId(rouletteDto);
		
		ModelAndView mav = new ModelAndView();

		// roulette_id에 따라 이벤트가 진행중이면 false -> 이벤트 종료되었습니다.
		PointRoulette pointRoulette = rouletteService.selectPointRoulette(roulette_id, request);
		
		if ("null".equals(String.valueOf(pointRoulette))) {
			// 이벤트가 기간이 지났어도 이미지는 띄워줘야 해서 정보는 가져온다.
			pointRoulette = rouletteService.pointRouletteNoChkDate(roulette_id, request);
			mav.addObject("pointRoulette", pointRoulette );
			mav.addObject("msg", "EndEvent");
			mav.setViewName("/clip/roulette/rouletteMain");
			return mav;
		}
		
		// Keytype에 따른 화면 분기
		
		if("1".equals(rouletteDto.getKeytype())){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
		}else if("3".equals(rouletteDto.getKeytype()) ){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			int agreeChk = rouletteService.agreeChk(rouletteDto);
			if(agreeChk == 0){
				logger.debug("ctn:"+rouletteDto.getCtn());
				return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=3&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain");	
			}
		}else if("2".equals(rouletteDto.getKeytype()) ){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			int agreeChk = rouletteService.agreeChk(rouletteDto);
			if(agreeChk == 0){
				logger.debug("ga_id:"+rouletteDto.getGa_id());
				return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=2&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain"+"&returnURL="+returnURL);	
			}
		}
		
		pointRouletteJoin = rouletteService.selectId(rouletteDto, pointRouletteJoin);

		mav.addObject("pointRoulette", pointRoulette);
		mav.addObject("rouletteDto", rouletteDto);
		
		// 1인당 1일 1회인지 체크 -> 오늘은 참여하셨네요. 내일 또 참여바랍니다.
		int dailyCnt = rouletteService.getDailyCntChk(rouletteDto);
		logger.debug("Daily Count By Id : "+dailyCnt+" Count");
		if ( 0 < dailyCnt ) {
			mav.addObject("msg", "complete");
			mav.setViewName("/clip/roulette/rouletteMain");
			return mav;
		}else{
			
			// 당첨값 셋팅
			PointRouletteJoin resultPointRouletteJoin = rouletteService.getPointRouletteJoin(pointRoulette, rouletteDto,
					pointRouletteJoin, request);
			
			mav.addObject("resultPointRouletteJoin", resultPointRouletteJoin);
			mav.addObject("msg", "");
			mav.setViewName("/clip/roulette/rouletteMain");
			return mav;
			
		}

		
		
	}
	
	
	
	/*
	 * 룰렛 메인페이지 - 1일 1회 제한을 풀어놓음
	 */
	/*@RequestMapping(value = "/roulette/rouletteMain2.do")
	public ModelAndView rouletteMain2(
			@RequestParam(required = true) String roulette_id
			, @RequestParam(required = true) String keyid
			, @RequestParam(required = true) String keytype
			, @RequestParam(required = false) String returnURL
			, RouletteDto temprouletteDto,
			PointRouletteJoin tempPointRouletteJoin, HttpServletRequest request) throws java.lang.Exception {
		logger.debug("rouletteMain2 Page start!!!");

		RouletteDto rouletteDto = temprouletteDto;
		
		logger.debug("Parameter ====================");
		logger.debug("roulette_id : "+roulette_id);
		logger.debug("keyid : "+keyid);
		logger.debug("keytype : "+keytype);
		logger.debug("returnURL : "+returnURL);

		if("".equals(keyid) || null == keyid){
			logger.debug("CLIPPOINTERROR 100:'keyid' parameter nothing");
			throw new java.lang.Exception();
		}
		if("".equals(roulette_id) || null == roulette_id){
			logger.debug("CLIPPOINTERROR 101:'roulette_id' parameter nothing");
			throw new java.lang.Exception();
		}
		
		PointRouletteJoin pointRouletteJoin = tempPointRouletteJoin;
		
		// id선택 및 입력
		rouletteDto = rouletteService.selectId(rouletteDto);
		
		ModelAndView mav = new ModelAndView();

		// roulette_id에 따라 이벤트가 진행중이면 false -> 이벤트 종료되었습니다.
		PointRoulette pointRoulette = rouletteService.selectPointRoulette(roulette_id, request);
		if ("null".equals(String.valueOf(pointRoulette))) {
			pointRoulette = rouletteService.pointRouletteNoChkDate(roulette_id, request);
			mav.addObject("pointRoulette", pointRoulette );
			mav.addObject("msg", "이벤트 기간이 종료되었습니다.");
			mav.setViewName("/clip/roulette/rouletteMain");
			return mav;
		}
		
		// Keytype에 따른 화면 분기
		
		if("1".equals(rouletteDto.getKeytype())){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
		}else if("3".equals(rouletteDto.getKeytype()) ){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			int agreeChk = rouletteService.agreeChk(rouletteDto);
			if(agreeChk == 0){
				return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=3&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain2");	
			}	
		}else if("2".equals(rouletteDto.getKeytype()) ){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			int agreeChk = rouletteService.agreeChk(rouletteDto);
			if(agreeChk == 0){
				return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=2&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain2");	
			}
		}else if("2".equals(rouletteDto.getKeytype()) && "test".equals(rouletteDto.getStatus())){ // test
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=2&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain2"+"&returnURL="+returnURL);	
		}
		
		pointRouletteJoin = rouletteService.selectId(rouletteDto, pointRouletteJoin);

		// 1인당 1일 1회인지 체크 false -> 오늘은 참여하셨네요. 내일 또 참여바랍니다.
		//int dailyCnt = rouletteService.getDailyCntChk(rouletteDto);
		// if ( 0 < dailyCnt ) {
		// mav.addObject("msg", "이미 참여하였습니다. 내일 다시 응모해주세요.");
		// mav.setViewName("/clip/roulette/rouletteMain");
		// return mav;
		// }

		
		// 당첨값 셋팅
		PointRouletteJoin resultPointRouletteJoin = rouletteService.getPointRouletteJoin(pointRoulette, rouletteDto,
				pointRouletteJoin, request);
		
		mav.addObject("pointRoulette", pointRoulette);
		mav.addObject("resultPointRouletteJoin", resultPointRouletteJoin);
		mav.addObject("rouletteDto", rouletteDto);
		mav.addObject("msg", "");
		mav.setViewName("/clip/roulette/rouletteMain2");
		return mav;
	}*/
	
	

	/*
	 * 룰렛 메인페이지에서 룰렛을 돌리는 순간 호출되는 클래스이다
	 * ( 룰렛을 돌리는 순간 모든 이벤트가 끝나며 화면에서는 룰렛이 멈춘 후 결과값을 보여준다 ) 
	 */
	@RequestMapping(value = "/roulette/rouletteComplete.do", method = { RequestMethod.POST })
	@ResponseBody
	public String rouletteComplete(RouletteDto temprouletteDto, PointRouletteJoin pointRouletteJoin,
			HttpServletRequest request, HttpServletResponse response

	) throws java.lang.Exception {
		logger.debug("rouletteComplete Page start!!!");
		
		RouletteDto rouletteDto = temprouletteDto;
		
		Map<String,String> map = new HashMap<String,String>();
		
		// 1인당 1일 1회인지 체크
		int dailyCnt = rouletteService.getDailyCntChk(rouletteDto);
		if ( 0 < dailyCnt ) {
			map.put("result", "complete");
			return JSONValue.toJSONString(map);
		}else{
			// 포인트 api 연동 + 연동로그 입력 + 당첨값 확정 update + 합계테이블 업데이트(있는지 확인)
			int result = rouletteService.rouletteComplete(rouletteDto, pointRouletteJoin, request);
			
			if (result == 1) {
				map.put("result", "success");
				return JSONValue.toJSONString(map);
			}else if (result == 2) {
				map.put("result", "max06Change");
				return JSONValue.toJSONString(map);
			}else if (result == 3) {
				map.put("result", "complete");
				return JSONValue.toJSONString(map);
			}else {
				map.put("result", "false");
				return JSONValue.toJSONString(map);
			}
		}
		

	}
	
	/*
	 * 룰렛 메인페이지에서 룰렛을 돌리는 순간 호출되는 클래스이다
	 * ( 룰렛을 돌리는 순간 모든 이벤트가 끝나며 화면에서는 룰렛이 멈춘 후 결과값을 보여준다 ) 
	 */
	/*@RequestMapping(value = "/roulette/rouletteComplete2.do", method = { RequestMethod.POST })
	@ResponseBody
	public String rouletteComplete2(RouletteDto temprouletteDto, PointRouletteJoin pointRouletteJoin,
			HttpServletRequest request, HttpServletResponse response

	) throws java.lang.Exception {
		logger.debug("rouletteComplete2 Page start!!!");

		RouletteDto rouletteDto = temprouletteDto;
		
		Map<String,String> map = new HashMap<String,String>();
		
		// 1인당 1일 1회인지 체크
		//int dailyCnt = rouletteService.getDailyCntChk(rouletteDto);
		//if ( 0 < dailyCnt ) {
		//	map.put("result", "false");
		//	return JSONValue.toJSONString(map);
		//}else{
			// 포인트 api 연동 + 연동로그 입력 + 당첨값 확정 update + 합계테이블 업데이트(있는지 확인)
			int result = rouletteService.rouletteComplete(rouletteDto, pointRouletteJoin, request);
			
			if (result == 1) {
				map.put("result", "success");
				return JSONValue.toJSONString(map);
			}else if (result == 2) {
				map.put("result", "max06Change");
				return JSONValue.toJSONString(map);
			} else {
				map.put("result", "false");
				return JSONValue.toJSONString(map);
			}
		//}
		

	}*/
	
	/*
	 * 개인정보 동의 페이지
	 */
	@RequestMapping(value = "/roulette/agreeForm.do")
	public ModelAndView agreeForm(
			@RequestParam(required = false) String returnPage,
			@RequestParam(required = true) String roulette_id,
			@RequestParam(required = true) String keyid, 
			@RequestParam(required = true) String keytype,
			@RequestParam(required = false) String returnURL, 
			RouletteDto temprouletteDto,
			PointRouletteJoin pointRouletteJoin, HttpServletRequest request) throws java.lang.Exception {
		logger.debug("agreeForm Page start!!!");

		RouletteDto rouletteDto = temprouletteDto;
		
		// id선택 및 입력
		rouletteDto = rouletteService.selectId(rouletteDto);
		
		ModelAndView mav = new ModelAndView();
		PointRoulette pointRoulette = rouletteService.selectPointRoulette(roulette_id, request);
		
		mav.addObject("pointRoulette", pointRoulette);
		mav.addObject("rouletteDto", rouletteDto);
		mav.addObject("msg", "");
		mav.setViewName("/clip/roulette/agreeForm");
		return mav;
	}
	
	/*
	 * 개인정보 동의 입력
	 */
	@RequestMapping(value = "/roulette/agreeInsert.do")
	public ModelAndView agreeInsert(
			@RequestParam(required = false) String returnPage,
			@RequestParam(required = true) String roulette_id,
			@RequestParam(required = true) String keyid, @RequestParam(required = true) String keytype,
			@RequestParam(required = false) String returnURL, RouletteDto temprouletteDto,
			HttpServletRequest request,
			ModelMap map) throws java.lang.Exception {
		logger.debug("agreeInsert Page start!!!");

		RouletteDto rouletteDto = temprouletteDto;
		
		// id선택 및 입력
		rouletteDto = rouletteService.selectId(rouletteDto);
		
		ModelAndView mav = new ModelAndView();
		
		// 개인정보 동의했다는 내용 입력
		//int result = rouletteService.agreeInsert(rouletteDto, request);
		rouletteService.agreeInsert(rouletteDto, request);
		
		// Keytype에 따른 화면 분기
		if("3".equals(rouletteDto.getKeytype())){
			map.addAttribute("msg", "동의가 완료되었습니다.");
			map.addAttribute("url", "/roulette/"+returnPage+".do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=3&keyid="+rouletteDto.getKeyid()+"&returnPage="+returnPage);
			mav.setViewName("/go-between");
		}else if("2".equals(rouletteDto.getKeytype())){
			map.addAttribute("msg", "동의가 완료되었습니다.");
			map.addAttribute("url", "/roulette/"+returnPage+".do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=2&keyid="+rouletteDto.getKeyid()+"&returnPage="+returnPage+"&returnURL="+returnURL);
			mav.setViewName("/go-between");
		}
				
		return mav;
	}
	
	
	/*
	 * SMS인증
	 */
	@RequestMapping(value = "/roulette/smsAuth.do", method = { RequestMethod.POST })
	@ResponseBody
	public String smsAuth(
			@RequestParam (required=true) String tel,
			@RequestParam (required=true) String roulette_id,
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("smsAuth Page start!!!");

		// 인증번호 DB에 저장하고 SMS보내기
		String result = rouletteService.smsAuth(tel, roulette_id, request);
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("result", result);
		return JSONValue.toJSONString(map);

	}
	
	/*
	 * SMS에서 받은 코드와 현재 입력한 코드 비교
	 */
	@RequestMapping(value = "/roulette/smsAuthChk.do", method = { RequestMethod.POST })
	@ResponseBody
	public String smsAuthChk(
			@RequestParam (required=true) String tel,
			@RequestParam (required=true) String roulette_id,
			@RequestParam (required=true) String smscode,
			HttpServletRequest request, HttpServletResponse response
	) throws java.lang.Exception {
		logger.debug("smsAuthChk Page start!!!");

		// 비교
		int result = rouletteService.smsAuthChk(tel, roulette_id, smscode, request);
		
		Map<String,String> map = new HashMap<String,String>();
		if (result == 1) {
			map.put("result", "success");
			return JSONValue.toJSONString(map);
		} else {
			map.put("result", "fail");
			return JSONValue.toJSONString(map);
		}
		
	}
	
	
}
