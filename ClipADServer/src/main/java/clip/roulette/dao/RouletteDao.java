/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.dao;

import clip.roulette.bean.PointApicallLog;
import clip.roulette.bean.PointRoulette;
import clip.roulette.bean.PointRouletteAuth;
import clip.roulette.bean.PointRouletteConnLog;
import clip.roulette.bean.PointRouletteJoin;
import clip.roulette.bean.PointRouletteJoinSum;
import clip.roulette.bean.PointRouletteOptin;
import clip.roulette.bean.PointRouletteSetLog;
import clip.roulette.bean.RouletteDto;

public interface RouletteDao   {

	PointRoulette selectPointRoulette(String roulette_id) throws java.lang.Exception;
	
	PointRoulette pointRouletteNoChkDate(String roulette_id)  throws java.lang.Exception;
	
	int getDailyCntChk(RouletteDto rouletteDto) throws java.lang.Exception;

	PointRoulette getSelectedPointRoulette(PointRoulette pointRoulette) throws java.lang.Exception;
	
	PointRoulette getSelectedPointRoulette2(PointRoulette pointRoulette) throws java.lang.Exception;

	int insertPointRouletteJoin(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception;

	void insertPointRouletteConnLog(PointRouletteConnLog pointRouletteConnLog) throws java.lang.Exception;

	int insertPointApicallLog(PointApicallLog pointApicallLog) throws java.lang.Exception;

	int updatePointRouletteJoin(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception;

	int selectPointRouletteJoinSumByDate(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception;

	void insertPointRouletteJoinSum(PointRouletteJoinSum pointRouletteJoinSum) throws java.lang.Exception;

	int updatePointRouletteJoinSum(PointRouletteJoinSum pointRouletteJoinSum) throws java.lang.Exception;

	int pointRouletteSetLogCnt(String roulette_id)  throws java.lang.Exception;

	void insertPointRouletteSetLog(PointRouletteSetLog pointRouletteSetLog) throws java.lang.Exception;

	void updatePointRoulette(PointRouletteSetLog pointRouletteSetLog) throws java.lang.Exception;

	int agreeInsert(PointRouletteOptin pointRouletteOptin) throws java.lang.Exception;

	int agreeChk(RouletteDto rouletteDto) throws java.lang.Exception;

	int smsAuthInsert(PointRouletteAuth pointRouletteAuth) throws java.lang.Exception;

	int smsAuthChk(PointRouletteAuth pointRouletteAuth)  throws java.lang.Exception;

	PointRouletteJoin selectPointRouletteJoin(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception;

	String selectRequesterCode(PointRouletteJoin pointRouletteJoin)  throws java.lang.Exception;
	
	String selectRequesterDescription(PointRouletteJoin pointRouletteJoin)  throws java.lang.Exception;

	int max06Chk(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception;

	int max06ChkByEndTime(RouletteDto rouletteDto) throws java.lang.Exception;

	int updatePointRouletteJoinSimple(String cust_id) throws java.lang.Exception;

	

	

	
}
