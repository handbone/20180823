/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.service;

import java.net.URLEncoder;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import clip.common.util.DateUtil;
import clip.common.util.HttpClientUtil;
import clip.common.util.TelegramUtil;
import clip.framework.BaseConstant;
import clip.framework.HttpNetwork;
import clip.roulette.bean.BuzzAdItem;
import clip.roulette.bean.BuzzvilMessage;
import clip.roulette.bean.PointBombHist;
import clip.roulette.bean.PointBombInfo;
import clip.roulette.dao.PointBombDao;

@Service
@Transactional
public class PointBombServiceImpl implements PointBombService {
	Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	//버즈빌 응답용 코드
	public static final String RESULT_SUCCESS = "00";
	public static final String RESULT_NO_INFO = "01";
	
	//확률분포 초기값
	@SuppressWarnings("unused")
	private static final int[][] pre = {{1,65},{2,40},{3,25},{4, 15},{5, 8},{6,7},{7,6},{8,6},{9,5},{10,5}
										,{11,4},{12,4},{13,3},{14,3},{15,3},{16,2},{17,2},{18,2},{19,1},{20,1}};

	private static int SERIAL_NO;
	
	@Autowired
	private PointBombDao pointBombDao;
	
	@Value("#{props['BUZZVIL_PARTICIPATE_URL']}")
	private String BUZZVIL_PARTICIPATE_URL;

	@Value("#{props['BUZZVIL_UNIT_ID']}")
	private String BUZZVIL_UNIT_ID;
	
	@Override
	public PointBombInfo updatePointBombInfoByLockScreenNoti(PointBombInfo pointBombInfo) throws Exception {	

		PointBombInfo dbData = pointBombDao.selectPointBombInfo(pointBombInfo);
		if(dbData == null) {
			PointBombInfo result = new PointBombInfo();
			result.setResult_code(RESULT_SUCCESS);
			
			//기존 정보가 없을때는 잠금을 켰을때만 인서트 처리하고 종료
			if("Y".equals(pointBombInfo.getUse_yn())) {
				pointBombDao.insertPointBombInfo(pointBombInfo);
				
				result.setDay_count(1);
			} else {
				result.setDay_count(0);
			}
			
			return result;
			
		} else {
			if("N".equals(pointBombInfo.getUse_yn())) {

				//잠금화면을 끈 경우는 현재 상태가 Y 경우만 처리하면 된다.
				if("Y".equals(dbData.getUse_yn())) {
					
					if(!DateUtil.getDate2String(DateUtil.addDate(1), "yyyyMMdd").equals(dbData.getBase_date()) ) {
						dbData.setBase_date(DateUtil.getToday("yyyyMMdd"));
					}
					dbData.setUse_yn(pointBombInfo.getUse_yn());
					dbData.setUse_start_date(DateUtil.getToday("yyyyMMddHHmmss"));
					dbData.setDirect_yn("N");
					dbData.setBanner_id("");
					dbData.setBase_point(0);
					dbData.setLevel(0);
					dbData.setBomb_count(0);
					
					pointBombDao.updatePointBombInfo(dbData);
				}
				
				dbData.setDay_count(0);
			} else {
				//잠금화면을 켠 경우는 현재 상태가 N 경우만 처리하면 된다.
				if("N".equals(dbData.getUse_yn())) {
					// 해당 base_date 컬럼이 day_count 에 영향을 미침
					// day_count가 0일차인 사용자(당일 폭탄 터트린 cust_id)는 day_count 변동 없이 진행
					if(!DateUtil.getDate2String(DateUtil.addDate(1), "yyyyMMdd").equals(dbData.getBase_date()) ) {
						dbData.setBase_date(DateUtil.getToday("yyyyMMdd"));
					}
					dbData.setUse_yn(pointBombInfo.getUse_yn());
					dbData.setUse_start_date(DateUtil.getToday("yyyyMMddHHmmss"));
					dbData.setDirect_yn("N");
					dbData.setBanner_id("");
					dbData.setBase_point(0);
					dbData.setLevel(0);
					dbData.setBomb_count(0);
					
					pointBombDao.updatePointBombInfo(dbData);
					
					if(!DateUtil.getDate2String(DateUtil.addDate(1), "yyyyMMdd").equals(dbData.getBase_date()) ) {
						dbData.setDay_count(1);	//새로 켜졌으므로 1
					}
				}
				
				
			}
			
			dbData.setResult_code(RESULT_SUCCESS);
		}
		
		return dbData;
	}

	@Override
	public PointBombInfo levelUp(PointBombInfo pointBombInfo) throws Exception {

		PointBombInfo dbData = pointBombDao.selectPointBombInfo(pointBombInfo);
		if(dbData == null || "N".equals(dbData.getUse_yn())) {
			PointBombInfo result = new PointBombInfo();
			result.setResult_code(RESULT_NO_INFO);
			return result;
		}

		if(dbData.getLevel() < 30){
			dbData.setLevel(dbData.getLevel() + 1);
			pointBombDao.updatePointBombInfo(dbData);
		}

		dbData.setResult_code(RESULT_SUCCESS);
		return dbData;
	}

	/**
	 *
	 * 2017.09.15  point bomb 정보 없을시  데이터 insert
	 */
	@Override
	public PointBombInfo getPointBombInfo(PointBombInfo pointBombInfo) throws Exception {
		PointBombInfo dbData = pointBombDao.selectPointBombDetailInfo(pointBombInfo);
		if(dbData == null) {
//			PointBombInfo result = new PointBombInfo();
//			result.setResult_code(RESULT_NO_INFO);
//			return result;
			pointBombInfo.setUse_yn("Y");
			pointBombDao.insertPointBombInfo(pointBombInfo);
			dbData = pointBombDao.selectPointBombDetailInfo(pointBombInfo);
		}
		
		dbData.setResult_code(RESULT_SUCCESS);
		return dbData;
	}

	@Override
	public List<BuzzAdItem> getOfferwallAdList() throws Exception {	
		List<BuzzAdItem> list = pointBombDao.selectBuzzAdItemList();
		
		if(list == null) {
			list = new ArrayList<BuzzAdItem>();
			return list;
		}
		
		//리스트 갯수가 적어서 추릴 필요가 없으면 바로 리턴
		if(list.size() <= 4)
			return list;
		
		int _size = list.size();
		List<BuzzAdItem> result = new ArrayList<BuzzAdItem>();
		
		int[] selected = randomRange(2, _size -1);
		
		//최소건 2개
		result.add(list.get(0));
		result.add(list.get(1));
		
		//랜덤 2개
		result.add(list.get( selected[0] ));
		result.add(list.get( selected[1] ));
		
		return result;
	}

	private static int[] randomRange(int min, int max) {
		
		int[] result = {0, 0};
		result[0] = (int) (Math.random() * (max - min + 1)) + min;
		
		for (int i=0; i<100; i++) {
			int second = (int) (Math.random() * (max - min + 1)) + min;
			if(result[0] != second) {
				result[1] = second;
				break;
			}
		}

		return result;
	}
	
	/*public static void main (String[] args) {
		try {
			int sum = 0;
			int count10 = 0;int count20 = 0;
			int count200 = 0;
			int count180 = 0;
			int count190 = 0;
			
			for (int i = 0; i < 1000000; i++) {
				double value = getRandomBombPoint(20);
				
				if (value == 10) {
					count10++;
				}  else if (value == 20) {
					count20++;
				}else if (value == 200) {
					count200++;
				} else if (value == 190) {
					count190++;
				} else if (value == 180) {
					count180++;
				}
				
				sum += value;
			}
			
			System.out.println("avg = " + (sum / 1000000.0));
			System.out.println("10 = " + count10);
			System.out.println("20 = " + count20);
			System.out.println("180 = " + count180);
			System.out.println("190 = " + count190);
			System.out.println("200 = " + count200);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	private static int getRandomBombPoint(int input) {
        SecureRandom random =  new SecureRandom();
        random.setSeed(System.currentTimeMillis());
        return  random.nextInt(input) +1;
	}
	
	/*
	private static int getRandomBombPoint(int input) {
		Random random = new Random();
		double r = random.nextGaussian();
		System.out.println(r);
		//while (r > 3.01) {
		//	r = random.nextGaussian();
		//}
		
		if(input > 130) {
			r += 0.5;
		} else if (input < 70) {
			r -= 0.5;
		}
		System.out.println(r);
		r = Math.min(4, Math.max(-4, r)) / 4;
		System.out.println(r);
	    int result = input + (int)(r >= 0 ? (r * (200 - input)) : (r * (input - 10)));
	    System.out.println(result);
	    return Math.min(200, Math.max(10, result));
	}
	*/
	
	@Override
	public PointBombInfo getPointRouletteJoin(PointBombInfo param) throws java.lang.Exception {
		PointBombInfo dbData = pointBombDao.selectPointBombDetailInfo(param);
		if(dbData == null ) {
			dbData = new PointBombInfo();
			dbData.setResult("F");
			dbData.setResultMsg("사용 권한이 없습니다.");
			return dbData;
		}
		
		if(dbData.getDay_count() != 3) {
			dbData = new PointBombInfo();
			dbData.setResult("F");
			dbData.setResultMsg("사용 권한이 없습니다.");
			return dbData;
		}
		
		if(dbData.getReward_point() <= 0) {
			int reward_point = getRandomBombPoint(15);
			dbData.setReward_point(reward_point);
			dbData.setReward_level(dbData.getLevel());	// 레벨은 나중에 굴릴떄 정하는 것으로 한다.
			
			if("Y".equals(dbData.getDirect_yn())) {
				dbData.setReward_base(dbData.getBase_point());
			}
			
			String temp_idx = getSerial();
			String tr_id = TelegramUtil.makeTrIdForClip(temp_idx);
			dbData.setTr_id(tr_id);
			
			pointBombDao.updatePointBombInfoForReward(dbData);
		} else {
			String temp_idx = getSerial();
			String tr_id = TelegramUtil.makeTrIdForClip(temp_idx);
			dbData.setTr_id(tr_id);
		}

		return dbData;
	}

	// 룰렛이벤트를 완료하는 로직
	@Override
	public PointBombInfo rouletteComplete(PointBombInfo param, HttpServletRequest request) throws java.lang.Exception {
		
		PointBombInfo dbData = pointBombDao.selectPointBombInfo(param);
		if(dbData == null) {
			dbData = new PointBombInfo();
			dbData.setResult("F");
			dbData.setResultMsg("사용 권한이 없습니다.");
			return dbData;
		}
		
		if(dbData.getReward_point() <= 0) {
			dbData = new PointBombInfo();
			dbData.setResult("F");
			dbData.setResultMsg("사용 권한이 없습니다.");
			return dbData;
		}

		StringBuffer log = new StringBuffer();
	
    	//log.append("\r\n===================================================================\r\n");
    	Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy.MM.dd HH:mm:ss.SSS");
    	log.append("Request Time=" + format.format(calendar.getTime()) + "\r\n");
		
		// 포인트 api 연동 
		String strDo = BaseConstant.PLUS_POINT;
		
		String idString = "cust_id="+param.getCust_id();

		String requester_code =  "pointbomb";		
		String requester_description = "3일마다 터지는 포인트 폭탄 이벤트";	
		
		//String temp_idx = getSerial();
		//String tr_id = TelegramUtil.makeTrIdForClip(temp_idx);
		//이전에 생성했던 TR_ID 를 사용하여 중복 방지
		String tr_id = param.getTr_id();
		
		log.append("TR_ID check start " + format.format(calendar.getTime()) + "=================================\r\n");
		if(tr_id == null || tr_id.isEmpty()) {
			String temp_idx = getSerial();
			tr_id = TelegramUtil.makeTrIdForClip(temp_idx);
			log.append("pointbomb :: TR_ID NULL! 생성완료 transactionId : " + tr_id + format.format(calendar.getTime()) + "\r\n");
		}
		
		
		int real_base_point = (dbData.getReward_base() * 80) / 100;
		int reward_point = dbData.getReward_point();
		int reward_level = dbData.getLevel();
		
		dbData.setReal_base_point(real_base_point);
		int total_reward = dbData.getReward_point() + dbData.getLevel() + real_base_point;
		
		String endUrl = idString
						+"&transaction_id="+tr_id
						+"&requester_code="+requester_code
						+"&point_value="+total_reward
						+"&description="+java.net.URLEncoder.encode(requester_description, BaseConstant.DEFAULT_ENCODING);
		endUrl = endUrl.replaceAll("(\r\n|\n)", "");
		endUrl = endUrl.replaceAll(System.getProperty("line.separator"), "");
		
		// Plus Point API 접속 URL
		logger.debug("Plus Point API URL : "+strDo+"?"+endUrl);
		
		String strHtmlSource = "";
		String errMsg = "";
		
		if ("0".equals(total_reward)){
			strHtmlSource = "No Request.";
		} else {
			
			try {
				// 실제 통신이 일어나는 부분
				strHtmlSource = httpNetwork.strGetData(strDo,endUrl);
				logger.debug("Plus Point API URL Response : "+strHtmlSource);  
			} catch (Exception e ) {
				// 통신에서 에러가 발생했을때 에러 메시지를 로그로 남긴다
				errMsg = e.getMessage();
				logger.debug("Plus Point API URL Response : "+errMsg);
				if(errMsg.contains("code: 600")){
					logger.debug("CLIPPOINTERROR 305:pointPlus http api error.");
	        	}else{
	        		logger.debug("CLIPPOINTERROR 300:pointPlus http api error.");
	        	}
			}
		}
		
    	log.append("Request URL:" + request.getRequestURL().toString() + "\r\n");
    	log.append("Plus Point API URL :" + strDo+ "\r\n");
    	log.append("Plus Point API Parameter [idString]:" + idString + "\r\n");
    	log.append("Plus Point API Parameter [transaction_id]:" + tr_id + "\r\n");
    	log.append("Plus Point API Parameter [requester_code]:" + requester_code + "\r\n");
    	log.append("Plus Point API Parameter [point_value]:" + total_reward+ "\r\n");
    	log.append("Plus Point API Parameter [description]:" + requester_description + "\r\n");
    	if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립(600)
    		log.append("Plus Point API Response :" + errMsg + "\r\n");
    	}else{
    		log.append("Plus Point API Response :" + strHtmlSource + "\r\n");
    	}
    	log.append("Request IP=" + request.getRemoteAddr() + "\r\n");
    	Calendar calendar2 = Calendar.getInstance();
        log.append("Response Time=" + format.format(calendar2.getTime()) + "\r\n");
        log.append("===================================================================\r\n");
        logger.info(log.toString());
        
        strHtmlSource = "OK";
        
        if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립
        	if(errMsg.contains("code: 600")){ // 중복적립
        		dbData.setResultMsg("중복적립.");
        	} else {
        		dbData.setResultMsg("포인트 적립 처리가 실패하였습니다.");
        	}
        	
        	dbData.setResult("F");
		} else {
			// 성공
	        //연동로그 입력
	        PointBombHist pointBombHist = new PointBombHist();
	        pointBombHist.setCust_id(param.getCust_id());
	        pointBombHist.setDirect_yn(dbData.getDirect_yn());
	        pointBombHist.setBanner_id(dbData.getBanner_id());
	        pointBombHist.setTr_id(tr_id);
	        pointBombHist.setPoint(dbData.getReward_point());
	        pointBombHist.setLevel(dbData.getReward_level());
	        pointBombHist.setBase_point(dbData.getReward_base());
	        
	        pointBombDao.insertPointBombHist(pointBombHist);

	        //포인트 폭탄 정보 초기화
	        dbData.setBase_date(DateUtil.getDate2String(DateUtil.addDate(1), "yyyyMMdd"));
	        dbData.setBanner_id("");
	        dbData.setBase_point(0);
	        dbData.setDirect_yn("N");
	        dbData.setReward_point(0);
	        dbData.setReward_level(0);
	        dbData.setLevel(0);
	        dbData.setReward_base(0);
	        dbData.setBomb_count(dbData.getBomb_count()+1);
	        
	        pointBombDao.updatePointBombInfoForReward(dbData);

	        //화면에 넘겨주기 위해 다시 셋팅
	        dbData.setReward_point(reward_point);
	        dbData.setReward_level(reward_level);
	        dbData.setReward_base(real_base_point);
	        
	        dbData.setResult("S");
	        dbData.setResultMsg("포인트가 정상적으로 적립되었습니다.");
		}
        
        return dbData;
	}
	
	private synchronized static String getSerial(){
		
		if(SERIAL_NO < 1000)
			SERIAL_NO = 1000;

		int temp = ++SERIAL_NO;
		if(temp == 10000)
			SERIAL_NO = 1000;
		
		return ""+temp;
	}

	@Override
	public PointBombInfo checkPointBombAuth(PointBombInfo pointBombInfo) throws Exception {
		PointBombInfo dbData = pointBombDao.selectPointBombDetailInfo(pointBombInfo);
		
		if(dbData == null){
			dbData = new PointBombInfo();
			dbData.setResult("F");
			dbData.setResultMsg("권한이 없습니다.");
		}
		
		if("Y".equals(dbData.getDirect_yn()) || dbData.getDay_count() == 3){
			dbData.setResult("S");
		} else {
			dbData.setResult("F");
			dbData.setResultMsg("이벤트에 참여해야 폭탄을 터트릴 수 있습니다.");
		}
		
		return dbData;
	}

	@Override
	public PointBombInfo checkOfferwallAdAuth(PointBombInfo pointBombInfo, HttpServletRequest request) throws Exception {

		PointBombInfo result = new PointBombInfo();
		
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("POST", BUZZVIL_PARTICIPATE_URL);
			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");

			if(StringUtils.isEmpty(pointBombInfo.getCust_id())) {
				throw new Exception("사용자 키가 전달되지 않음");
			}

			conn.addParameter("unit_id", BUZZVIL_UNIT_ID);
			conn.addParameter("campaign_id", pointBombInfo.getBanner_id());
			conn.addParameter("ifa", pointBombInfo.getGa_id());
			conn.addParameter("custom", URLEncoder.encode(pointBombInfo.getCust_id(),BaseConstant.DEFAULT_ENCODING));
			conn.addParameter("client_ip", request.getRemoteAddr());
			conn.addParameter("user_id", ""+ URLEncoder.encode(pointBombInfo.getCust_id(),BaseConstant.DEFAULT_ENCODING));

			logger.info("PointBomb checkOfferwallAdAuth request cust_id[" + pointBombInfo.getCust_id()+"], ga_id[" + pointBombInfo.getGa_id()+"], banner id["+pointBombInfo.getBanner_id()+"]");
			
			int ret = conn.sendRequest();
			
			logger.info("PointBomb checkOfferwallAdAuth response status : " + ret);

			if(HttpStatus.SC_OK == ret) {
				
				BuzzvilMessage buzmsg = conn.getResponseJson(BuzzvilMessage.class);
				logger.debug("buz result : "+buzmsg.getMsg()+"("+buzmsg.getCode()+")");
				
				if(buzmsg.getCode() == 200){
					result.setResult("S");
					result.setLanding_url(buzmsg.getLanding_url());
					logger.info("PointBomb checkOfferwallAdAuth Success Response : landing_url["+buzmsg.getLanding_url()+"] ");
				} else {
					result.setResult("F");
					
					if("9013".equals(buzmsg.getCode()) || "9014".equals(buzmsg.getCode())) {
						result.setResultMsg("종료된 광고입니다.");
					} else if("9020".equals(buzmsg.getCode())) {
						result.setResultMsg("이미 참여한 광고입니다.");
					} else if("9021".equals(buzmsg.getCode())) {
						result.setResultMsg("이미 소진한 광고입니다.");
					} else if("9008".equals(buzmsg.getCode())) {
						result.setResultMsg("유효하지 않은 광고입니다.");
					} else {
						result.setResultMsg("처리도중 오류가 발생하였습니다.");
					}
					logger.info("PointBomb checkOfferwallAdAuth Error Fail Response : retcode["+buzmsg.getCode()+"] ");
				}
				
			} else {
				result.setResult("F");
				result.setResultMsg("처리도중 오류가 발생하였습니다.");
				logger.info("PointBomb checkOfferwallAdAuth Error HTTP : code["+ret+"] ");
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			result.setResult("F");
			result.setResultMsg("처리도중 오류가 발생하였습니다.");
			logger.info("PointBomb checkOfferwallAdAuth Error System : message["+e.getMessage()+"] ");
		}
		
		return result;
		
	}

}
