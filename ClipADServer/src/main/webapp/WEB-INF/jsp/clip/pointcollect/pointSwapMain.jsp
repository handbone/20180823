<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">


</head>
<body>
<form id="form" name="form" action="/clip/pointcollect/pointSwapComplete.do">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden"  id="debug" name="debug" value="${debug}"/>
	<input type="hidden"  id="resultCardNames" name="cardNames" value=""/>
	<input type="hidden"  id="resultPoints" name="points" value=""/>
	<input type="hidden"  id="resultResults" name="results" value=""/>
	<input type="hidden"  id="pointOvers" name="pointOvers" value=""/>
	
	<input type="hidden"  id="pageType" name="pageType" value="2" />
</form>

<div class="wrap">

	<!-- header -->
	<div class="header">
		<h1 class="page_title">포인트 전환</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					if ( document.referrer.indexOf("pointSwapMain.do") > -1){
						window.history.go(-2);
					} else {
						window.history.back();
					}
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back" ><span>뒤로가기</span></a>
	</div><!-- // header -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				분산되어 있는 신용카드 포인트를 모아 모아 <br>
				클립포인트로 전환하세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="movePage('<c:url value="/pointmain/main.do" />', 'form');">
				<!-- 170719 수정 -->
				<span class="title">클립포인트</span>
				<!-- // 170719 수정 -->
				<strong class="point usepointValue"><span id="mypoint">0</span><span>P</span></strong>
				<!-- <span id="mypoint" class="usepointValue">0</span> -->
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<!--  신용카드/멤버십 포인트  -->
		<div class="section">
			<div class="section_inner">
				<h2 class="section_title">신용카드/멤버십 포인트</h2>
						<!--  신한카드  -->
						<div class="card_point_box shinhan" <c:if test="${contractError eq 'Y' or contractInfoShinhan.userAgreeYN eq 'N' }"> style="background-color :gray;"  onclick="javascript:fnPopupAgreeCheck();"</c:if> >
							<div class="card_name_box">
								<span class="card_name card_shihan">
									<img src="../images/temp/card_shinhan.png" alt="신한fan클럽">
								</span>
								<span class="exp_txt">전환한도 10만원/일</span>
							</div>	
							<div class="point_area">
								<div class="inr_top">
									<span class="tit">보유 마이 신한 포인트</span>
									<em class="cash"><strong class="shinhanPoint">0</strong><span>P</span></em>
								</div>
								<div class="inr_bot">
									<span class="tit">전환 포인트</span>
									<div class="ip_point_box"  <c:if test="${contractError ne 'Y' and contractInfoShinhan.userAgreeYN eq 'Y' }"> onclick="javascript:fnPopupSetting('shinhan');"</c:if> >
<!-- 										<div class="ip_point_box"  onclick="javascript:fnPopupSetting('shinhan');"> -->
										<em>0</em>
										<span>P</span>
										<input type="hidden" class="cardNames" name="cardNames" id="shinhan" value="shinhan" /> 
										<input type="hidden" class="points" name="point" value="0" /> 
									</div>
								</div>
							</div>
						</div>
						<!--  // 신한카드  -->
						<!-- 하나카드 -->
						<div class="card_point_box hana" <c:if test="${contractError eq 'Y' or contractInfoHana.userAgreeYN eq 'N' }"> style="background-color :gray;"  onclick="javascript:fnPopupAgreeCheck();"</c:if> >
							<div class="card_name_box">
								<span class="card_name card_hana"><img src="../images/temp/card_hana.png" alt="하나카드"></span>
								<span class="exp_txt">전환한도 50만원/월, 10회/일</span>
							</div>
							<div class="point_area">
								<div class="inr_top">
									<span class="tit">보유 하나머니</span>
									<em class="cash"><strong class="hanaPoint" >0</strong><span>원</span></em>
								</div>
								<div class="inr_bot">
									<span class="tit">전환 포인트</span>
									<div class="ip_point_box" <c:if test="${contractError ne 'Y' and contractInfoHana.userAgreeYN eq 'Y' }"> onclick="javascript:fnPopupSetting('hana');"</c:if>>
<!-- 										<div class="ip_point_box" onclick="javascript:fnPopupSetting('hana');"> -->
										<em>0</em>
										<span>P</span>
										<input type="hidden" class="cardNames" name="cardNames" id="hanamembers" value="hanamembers" /> 
										<input type="hidden" class="points" name="point" value="0" /> 
									</div>
								</div>
							</div>
						</div>
						<!-- // 하나카드 -->		
						<!-- BC카드 -->
						<!-- 약관동의가 없으면 안나오도록 null이면 안보이게 c:if 추가 -->
						<div class="card_point_box bc" <c:if test="${contractError eq 'Y' or contractInfoBccard.userAgreeYN eq 'N' }"> style="background-color :gray;"  onclick="javascript:fnPopupAgreeCheck();"</c:if>>
							<div class="card_name_box">
								<span class="card_name card_bc"><img src="../images/temp/card_bccard.png" alt="BC카드"></span>
								<!-- <span class="exp_txt">전환한도 30만원/월</span> -->
							</div>
							<div class="point_area">
								<div class="inr_top">
									<span class="tit">보유 TOP 포인트</span>
									<em class="cash"><strong class="bcPoint" >0</strong><span>P</span></em>
								</div>
								<div class="inr_bot">
									<span class="tit">전환 포인트</span>
									<div class="ip_point_box"  <c:if test="${contractError eq 'N' or contractInfoBccard.userAgreeYN eq 'Y' }"> onclick="javascript:fnPopupSetting('bc');" </c:if> >
<!-- 										<div class="ip_point_box"  onclick="javascript:fnPopupSetting('bc');" > -->
										<em>0</em>
										<span>P</span>
										<input type="hidden" class="cardNames" name="cardNames" id="bccard" value="bccard" /> 
										<input type="hidden" class="points" name="point" value="" /> 
									</div>
								</div>
							</div>
						</div>
						<!-- BC카드 -->
						
						<!-- KB카드 -->
						<div class="card_point_box kb" 
 						<c:if test="${contractError eq 'Y' or contractInfoKbcard.userAgreeYN eq 'N' }">  
						style="background-color :gray;"  onclick="javascript:fnPopupAgreeCheck();"
 						</c:if>
						>
							<div class="card_name_box">
								<span class="card_name card_kb"><img src="../images/temp/card_kb_livemate.png" alt="KB카드"></span>
								<span class="exp_txt">전환한도 30만원/월</span>
							</div>
							<div class="point_area">
								<div class="inr_top">
									<span class="tit">보유 포인트리</span>
									<em class="cash"><strong class="kbPoint" >0</strong><span>P</span></em>
								</div>
								<div class="inr_bot">
									<span class="tit">전환 포인트</span>
										<div class="ip_point_box" 
										<c:if test="${contractError ne 'Y' and contractInfoKbcard.userAgreeYN ne 'N' }"> 
										onclick="javascript:fnPopupSetting('kb');" 
										</c:if>
										>
											<em>0</em>
											<span>P</span>
											<input type="hidden" class="cardNames" name="cardNames" id="kbpointree" value="kbpointree" /> 
											<input type="hidden" class="points" name="point" value="" /> 
										</div>
								</div>
							</div>
						</div>
						<!-- //KB카드 -->
						
	<%-- 				
				<div class="card_point_box bc" <c:if test="${contractError eq 'Y' or contractInfoBccard.userAgreeYN eq 'N' }"> style="background-color :gray;"  onclick="javascript:fnPopupAgreeCheck();"</c:if>>
					<div class="card_name_box">
						<span class="card_name card_bc"><img src="../images/temp/card_bccard.png" alt="BC카드"></span>
						<!-- <span class="exp_txt">전환한도 30만원/월</span> -->
					</div>
					<div class="point_area">
						<div class="inr_top">
							<span class="tit">보유 TOP 포인트</span>
							<em class="cash"><strong class="bcPoint" >0</strong><span>P</span></em>
						</div>
						<div class="inr_bot">
							<span class="tit">전환 포인트</span>
							<c:if test="${contractError eq 'N' or contractInfoBccard.userAgreeYN eq 'Y'}">
								<div class="ip_point_box" onclick="javascript:fnPopupSetting('bc');">
							</c:if>
							<c:if test="${contractError eq 'Y' or contractInfoBccard.userAgreeYN eq 'N' }">
								<div class="ip_point_box">
							</c:if>
									<em>0</em>
									<span>P</span>
									<input type="hidden" class="cardNames" name="cardNames" value="bccard" /> 
									<input type="hidden" class="points" name="point" value="" /> 
								</div>
						</div>
					</div>
		--%>	
				<!-- </div> -->
				<!-- //BC카드 -->
				<%--
				<!-- NH농협카드 -->
				<!-- <div class="card_point_box">
					<div class="card_name_box">
						<span class="card_name card_nh"><img src="../images/temp/card_nh.png" alt="NH농협카드"></span>
					</div>
					<a href="javascript:;" class="btn_srh">조회하기</a>
				</div> -->
				<!-- //NH농협카드 -->
				<!-- LOTTE CARD -->
				<!-- <div class="card_point_box">
					<div class="card_name_box">
						<span class="card_name card_lotte"><img src="../images/temp/card_lotte.png" alt="CARD"></span>
					</div>
					<a href="javascript:;" class="btn_srh">조회하기</a>
				</div> -->
				<!-- //LOTTE CARD -->
				--%>
				<div class="point_two_wrap">
					<div class="point_two_box">
						<div class="inr">
							<span class="tit">보유 포인트 합계</span>
							<strong class="cash"><strong class="sumPoint" >0</strong><span>P</span></strong>
						</div>
						<div class="inr">
							<span class="tit">전환 포인트 합계</span>
							<strong class="cash"><strong class="chgPoint" >0</strong><span>P</span></strong>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  //신용카드/멤버십 포인트  -->
		<div class="section">
			<div class="section_inner">
				<!-- 포인트 전환 -->
				<div class="point_transform_wrap">
					<p class="top_title">
						<strong class="point"><strong class="chgPoint" >0</strong><span>P</span></strong>를 클립포인트로<br>
						전환 하시겠습니까?
					</p>
					<div class="btn_multi_box">
						<button type="button" class="btn_cancel" onclick="javascript:history.go(-1);">취소</button>
						<button id="confirm_button" type="button" class="btn_confirm">확인</button>
						<!-- <button type="button" class="btn_confirm" onclick="javascript:fnPointSwap('Y');">확인</button> -->
					</div>
					<dl class="caution_wrap">
						<dt>주의 사항</dt>
						<dd>포인트가 전환되면, 클립포인트 운영약관에 따라 포인트 운영이 됩니다.</dd>
						<dd>포인트 전환은 실시간으로 진행 되며, 전환 이후 취소는 불가 합니다.</dd>
						<dd>전체 포인트 전환  총 한도는 일별  1,000,000P로 제한 됩니다.</dd>
						<dd>포인트 전환과 관련한 문의 사항은 클립앱내 1:1 문의에 남겨 주시기 바랍니다.</dd>
					</dl>
				</div>
				<!-- // 포인트 전환 -->
			</div>
			<a href="javascript:void(0);" onclick="window.location='KTolleh00114://settings/detail/applock'">
				<div class="passwrod_wrap">
					<span class="tit">Tip. 암호 잠금</span>
					<p class="cont_text">
						고객님의 정보 및 포인트를 안전하게 지키기 위해 <br>앱 잠금 서비스를 제공하고있습니다.
					</p>
					<span class="btn_view">앱 잠금설정 하기</span>
				</div>
			</a>
		</div>
	</div><!-- // contents -->

	
	
	<!--  popup 포인트 전환 -->
	<div class="layer_pop_wrap swapPoint">
		<div class="deem"></div>
		<%-- 신한포인트 --%>
		<div class="layer_pop" id="shinhanPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title"><img src="../images/temp/card_shinhan.png" alt="마이신한포인트" class="sinhancard"></strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash"><em class="shinhanPoint" >0</em><span>P</span></span>
						<div class="ip_point_box">
							<!-- [jaeheung] 2017.08.08 테스트용(input type을 text에서 number로 변경 후 키업 이벤트 삭제) -->
							<input type="number" pattern="[0-9]*" inputmode="numeric" min="0" value="0" title="전환포인트" placeholder="포인트 입력" onkeyup="" onclick="fnClear(this);" >
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<!-- <button type="button" class="btn_lp_cancel" onclick="javascript:layerAct.close('layer_pop_wrap');">취소</button>[D]2017.07.14 수정
					<button type="submit" class="btn_lp_confirm">확인</button> -->
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
		<%-- //신한포인트 --%>
		<%-- 하나포인트 --%>
		<div class="layer_pop" id="hanaPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title">
				<img src="../images/temp/card_hana.png" alt="하나카드" class="sinhancard">
				</strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash"><em class="hanaPoint" >0</em><span>P</span></span>
						<div class="ip_point_box">
							<input type="number" pattern="[0-9]*" inputmode="numeric" min="0" value="0" title="전환포인트" placeholder="포인트 입력" onkeyup="" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
		<%-- //하나포인트 --%>
		<%-- BC포인트 --%>
		<div class="layer_pop" id="bcPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title">
				<img src="../images/temp/card_bccard.png" alt="BC카드" class="sinhancard">
				</strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash"><em class="bcPoint" >0</em><span>P</span></span>
						<div class="ip_point_box">
							<input type="number" pattern="[0-9]*" inputmode="numeric" min="0" value="0" title="전환포인트" placeholder="포인트 입력" onkeyup="" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
		<%-- //BC포인트 --%>
		<div class="layer_pop" id="kbPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title">
				<img src="../images/temp/card_kb.png" alt="KB국민카드" class="sinhancard">
				</strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash"><em class="kbPoint" >0</em><span>P</span></span>
						<div class="ip_point_box">
							<input type="number" pattern="[0-9]*" inputmode="numeric" min="0" value="0" title="전환포인트" placeholder="포인트 입력" onkeyup="" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
	</div>
	<!--  //popup 포인트 전환 -->
	<!--  popup -->
	<div class="layer_pop_wrap pop_default">
		<div class="deem"></div>
		<div class="layer_pop">
			<div class="lp_hd">
				<strong class="title">마이신한포인트 알림</strong>
			</div>
			<div class="lp_ct">
				<div class="point_tf_cont">
					<div class="inr ex_txt">
						입력한 마이신한포인트는 신한FAN 클럽 회원에 
						한하여 포인트를 전환하실 수 있습니다.<br><br>

						고객님은 신한 FAN 클럽 회원이 아니므로, App을 
						다운로드하여 신한FAN 클럽에 가입후 다시 시도
						해주세요.<br><br>

						마이신한포인트를 0p로 설정하시면 
						다른 카드포인트는 전환이 가능합니다.
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:layerAct.close('pop_default')">취소</button>
					<button type="submit" class="btn_lp_confirm" onclick="javascript:goDown('shinhanFan');">App 다운로드</button>
				</div>
			</div>
		</div>
	</div>
	<!--  //popup -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!--  180119 popup 추가-->
	<%@include file="/WEB-INF/jsp/clip/alert_popup.jsp"%>
	<!--  //180119 popup 추가 -->
</div><!-- // wrap -->
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/pointCommon.js"></script>

<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script src="../js/dummy/dummy.js"></script>
<script type="text/javascript">
//180119 jyi 추가
function popup_confirm_Act(){
	var type = $('#popup_type').val();
	if(type == 'fnAgree'){
		$('#alert_popup').hide();
		window.location='webtoapp://clippoint/termagree?groupCode=GRTRM00001';
	}else if(type == 'fnSetting'){
		$('#alert_popup').hide();
		var data = $("#confirmSetting").attr("data");
		$("#"+data+"PopDiv .ip_point_box").find("input").val('');
		$("#"+data+"PopDiv .ip_point_box").find("input").focus();
	}
	else{
		$('#alert_popup').hide();
	}
	
}
// //180119 jyi 추가
	common.invoker.invoke("myPoint");
	
	var shinhanresult = '';
	var click_lock = true;
	
	$(document).ready(function(){
		
		//2018.04.11 jyi
		var userAgent = navigator.userAgent.toLowerCase();
		var osType = null;
	  	if ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1)	|| (userAgent.search("ipad") > -1)){
	    		osType = "IOS";
	    	}else{
	    		osType = "ANDROID";
	    	}
		var obj = new Object();
		obj.custid = $("#cust_id").val();
		obj.os = osType;
		obj.cid = "PNT001";
		sendMidas(obj);
		//\\2018.04.11 jyi
		
		goSearch();  // 코드 세팅
		//shinhanresult = chkShinhanFan();
		$('#confirm_button').on("click", function(){
			if(click_lock){
				click_lock = false;
				chkShinhanFan();
				
			}
		});
		
	});
	
	var goSearch = function(){

		var paramData = {};		
		paramData.user_ci = $("#user_ci").val();
		paramData.cust_id = $('#cust_id').val();
		
	    $.ajax({
	    	url: "/clip/pointcollect/getCardPoints.do",
	        type: "POST",
	        data: JSON.stringify(paramData),
	        dataType: "json",
	        contentType: 'application/json; charset=UTF-8',
	        success: function (resData) {
	        	
	            poInfo = resData.pointInfo;
	            
	            var kbPoint = poInfo.kbPoint.point;
	            var bcPoint = poInfo.bcPoint.point;
	            var hanaPoint = poInfo.hanaPoint.point;
	            var shinhanPoint = poInfo.shinhanPoint.point; 
	            
	            if(poInfo.hanaPoint.point == null){
	            	hanaPoint = 0;
	            }
	            if(poInfo.kbPoint.point == null){
	            	kbPoint = 0;
	            }
	            if(poInfo.bcPoint.point == null){
	            	bcPoint = 0;
	            }
	            if(poInfo.shinhanPoint.point == null){
	            	shinhanPoint = 0;
	            }
	            
	            var sumPoint = 0; 
       			<c:if test="${contractError ne 'Y' and contractInfoHana.userAgreeYN eq 'Y' }">     
            	$(".hanaPoint").text(comma(hanaPoint));
//             	$(".hana .ip_point_box").find("em").text(comma(hanaPoint));
//             	$(".hana .points").val(uncomma(hanaPoint));
            	sumPoint = Number(sumPoint) + Number(hanaPoint);
            	</c:if>
            	<c:if test="${contractError ne 'Y' and contractInfoKbcard.userAgreeYN eq 'Y' }">
            	$(".kbPoint").text(comma(kbPoint));
//             	$(".kb .ip_point_box").find("em").text(comma(kbPoint));
//             	$(".kb .points").val(uncomma(kbPoint));
            	sumPoint = Number(sumPoint) + Number(kbPoint);
            	</c:if>
            	
            	<c:if test="${contractError ne 'Y' and contractInfoBccard.userAgreeYN eq 'Y' }">
                	$(".bcPoint").text(comma(bcPoint));
//             	$(".kb .ip_point_box").find("em").text(comma(kbPoint));
//             	$(".kb .points").val(uncomma(kbPoint));
           	 	sumPoint = Number(sumPoint) + Number(bcPoint); 	
            	</c:if>

            	<c:if test="${contractError ne 'Y' and contractInfoShinhan.userAgreeYN eq 'Y' }">
            	$(".shinhanPoint").text(comma(shinhanPoint));
//             	$(".shinhan .ip_point_box").find("em").text(comma(shinhanPoint));
//             	$(".shinhan .points").val(uncomma(shinhanPoint));
            	sumPoint = Number(sumPoint) + Number(shinhanPoint);
            	</c:if>   
	            sumPoint = common.string.setComma(sumPoint);
	            
	            $(".sumPoint").text(sumPoint); 
// 	            $(".chgPoint").text(sumPoint); 
	            
	        },
	        beforeSend: function () {
				loadingClip.openClip();
	        },
	        complete : function() {
	        	loadingClip.closeClip();
	        },
	        error: function (e) {
	            console.log("error:" + e.responseText);
	            /* alert("네트워크 오류가 발생하였습니다."); */
	            // 180119 jyi 추가
		    		var _text = '네트워크 오류가 발생하였습니다.';
				$('#notice_word').empty();
				$('#notice_word').append(_text);
				$('#popup_type').val('default');
				$('#alert_type').hide();
				$('#alert_popup').show();
				// //180119 jyi 추가
	        }
	    });
	}
	
	var goSwap = function(){
		
		var paramData = {
			"cardNames" : [],
			"points" : []
		};
			
		var cardNames = [];
		var points = [];
		var totalPoints = 0;
		
		$(".contents .ip_point_box .cardNames").each(function(){
			
// 			cardNames.push($(this).val());
			cardNames.push($(this).attr('id'));
				
		}); 
		$(".contents .ip_point_box .points").each(function(){
			
// 			points.push($(this).val());
// 			totalPoints += Number($(this).val());
			points.push($(this).attr('id'));
			totalPoints += Number($(this).attr('id'));
				
		});
		
		if(totalPoints == 0) {
			click_lock = true;			
			/* alert('포인트를 입력해 주세요.'); */
			// 180119 jyi 추가
	    		var _text = '포인트를 입력해 주세요.';
			$('#notice_word').empty();
			$('#notice_word').append(_text);
			$('#popup_type').val('default');
			$('#alert_type').hide();
			$('#alert_popup').show();
			// //180119 jyi 추가
			return;
		}
		
		paramData.user_ci= $("#user_ci").val();
		paramData.cust_id = $('#cust_id').val();
		paramData.hanaPoint = {};
		paramData.hanaPoint.point = $(".contents .ip_point_box .points").eq(1).attr('id');
		paramData.shinhanPoint = {};
		paramData.shinhanPoint.point = $(".contents .ip_point_box .points").eq(0).attr('id');
		paramData.kbPoint = {};
		paramData.kbPoint.point = $(".contents .ip_point_box .points").eq(3).attr('id');
		paramData.bcPoint = {};
		paramData.bcPoint.point = $(".contents .ip_point_box .points").eq(2).attr('id');
		
		paramData.cardNames = cardNames;
		paramData.points = points;
		
		
		$.ajax({
	    	url: "/clip/pointcollect/swapCardPoints.do",
	    	type: "POST",
	        data: JSON.stringify(paramData),
	        dataType: "json",
	        contentType: 'application/json; charset=UTF-8',
	        success: function (resData) {
	            
	            var chkResult = resData.pointInfo.result;
	            var chkResultMsg = resData.pointInfo.resultMsg;
	            var hanaPointInfo = resData.pointInfo.hanaPoint;
	            var bcPointInfo = resData.pointInfo.bcPoint;
	            var kbPointInfo = resData.pointInfo.kbPoint;
	            var shinhanPoint = resData.pointInfo.shinhanPoint;
	            
	            //test TODO SUNNY
	            /* if(chkResult != 'F'){ */
	            if(chkResult == 'F'){
	            	//리절트 결과 알람
	            	/* alert(chkResultMsg); */
	            	// 180119 jyi 추가
		    		var _text = chkResultMsg;
				$('#notice_word').empty();
				$('#notice_word').append(_text);
				$('#popup_type').val('default');
				$('#alert_type').hide();
				$('#alert_popup').show();
				// //180119 jyi 추가
	            	click_lock = true;
	            	return;
	            }else{
	            	//결과화면으로 이동
					var resultPoints = [];
					var resultResults = [];
					var pointOver = [];
					
					resultPoints.push(shinhanPoint.point);
					resultPoints.push(hanaPointInfo.point);
					resultPoints.push(bcPointInfo.point);
					resultPoints.push(kbPointInfo.point);
					
					resultResults.push(shinhanPoint.result);
					resultResults.push(hanaPointInfo.result);
					resultResults.push(bcPointInfo.result);					
					resultResults.push(kbPointInfo.result);

					pointOver.push(shinhanPoint.pointOver);
					pointOver.push(hanaPointInfo.pointOver);
					pointOver.push(bcPointInfo.pointOver);
					pointOver.push(kbPointInfo.pointOver);
					
	            	fngoComfirm(cardNames, resultPoints, resultResults, pointOver);
	            	click_lock = true;
	            }
	            
	        },
	        beforeSend: function () {
				/* alert(this.data); */
	        },
	        error: function (e) {
	        	click_lock = true;
	            console.log("error:" + e.responseText);
	            /* alert("네트워크 오류가 발생하였습니다."); */
	            // 180119 jyi 추가
		    		var _text = "네트워크 오류가 발생하였습니다.";
				$('#notice_word').empty();
				$('#notice_word').append(_text);
				$('#popup_type').val('default');
				$('#alert_type').hide();
				$('#alert_popup').show();
				// //180119 jyi 추가
	        }
	    });
	}
		
	var chkShinhanFan = function(){
		var _shiPo = $(".contents .ip_point_box .points").eq(0).val();
		var chkInfo = '';
		if(_shiPo > 0) {
			var paramData = {};		
			paramData.user_ci = $("#user_ci").val();
			paramData.cust_id = $('#cust_id').val();
			
		    $.ajax({
		    	url: "/clip/pointcollect/checkShinhanFan.do",
		        type: "POST",
		        data: JSON.stringify(paramData),
		        dataType: "json",
		        contentType: 'application/json; charset=UTF-8',
		        success: function (resData) {
					chkInfo = resData.result;
					
					if(chkInfo == 'Y'){
						goSwap();
					} else if(chkInfo == 'N') {
						$(".pop_default").show();
						click_lock = true;
					} else {
						/* alert("네트워크 오류가 발생하였습니다."); */
						// 180119 jyi 추가
				    		var _text = "네트워크 오류가 발생하였습니다.";
						$('#notice_word').empty();
						$('#notice_word').append(_text);
						$('#popup_type').val('default');
						$('#alert_type').hide();
						$('#alert_popup').show();
						// //180119 jyi 추가
						click_lock = true;
					}
				},
		        beforeSend: function () {
					loadingClip.openClip();
		        },
		        complete : function() {
					loadingClip.closeClip();
				},
		        error: function (e) {
		        	click_lock = true;
					console.log("error:" + e.responseText);
					/* alert("네트워크 오류가 발생하였습니다."); */
					// 180119 jyi 추가
			    		var _text = "네트워크 오류가 발생하였습니다.";
					$('#notice_word').empty();
					$('#notice_word').append(_text);
					$('#popup_type').val('default');
					$('#alert_type').hide();
					$('#alert_popup').show();
					// //180119 jyi 추가
				}
		    });
		    
		}else{
			goSwap();
		}
		return chkInfo;
	}
	
	function fnPopupSetting(obj){
	
		$("#confirmSetting").attr("data",obj);
		
		$("#"+obj+"PopDiv").show();
		
		layerAct.open('swapPoint');
		$('#'+obj+'PopDiv').find('input').val('');
		$('#'+obj+'PopDiv').find('input').focus();
	}
	
	// 180119 jyi
	function fnPopupAgreeCheck() {
		/* if( confirm("아직 사용동의 전 입니다 동의 창으로 이동 하시겠습니까 ? ") ){
			window.location='webtoapp://clippoint/termagree?groupCode=GRTRM00001';
		} */
		var _text = '아직 사용동의 전 입니다 동의 창으로 이동 하시겠습니까 ? ';
		$('#notice_word').empty();
		$('#notice_word').append(_text);
		$('#popup_type').val('fnAgree');
		$('#alert_type').show();
		$('#alert_popup').show();
	}
	// //180119 jyi
	
	
	function fnSetting(obj){
		if(obj == 'Y'){
			
			var data = $("#confirmSetting").attr("data");
			var point = '0';
			if($("#"+data+"PopDiv .ip_point_box").find("input").val() != '') {
				point = $("#"+data+"PopDiv .ip_point_box").find("input").val();
			}
			var allPoint = $("#"+data+"PopDiv .cash").text();
			
			if(Number(uncomma(point)) > Number(uncomma(allPoint))){
				/* alert("보유 포인트를 초과하였습니다."); */
				// 180119 jyi 추가
				$("#"+data+"PopDiv .ip_point_box").find("input").blur();
		    		var _text = "보유 포인트를 초과하였습니다.";
				$('#notice_word').empty();
				$('#notice_word').append(_text);
				$('#popup_type').val('fnSetting');
				$('#alert_type').hide();
				$('#alert_popup').show();
				// //180119 jyi 추가
				/* $("#"+data+"PopDiv .ip_point_box").find("input").val('');
				$("#"+data+"PopDiv .ip_point_box").find("input").focus(); */
				return;
			}
			
			$("."+data+" .ip_point_box").find("em").text( comma(Number(point)) );
			$("."+data+" .points").val(uncomma(point));
			$("."+data+" .points").attr('id', uncomma(point));
			
			
			var chgPoint = 0;
			
			//전환포인트 합계 변경
			$(".contents .ip_point_box").find("em").each(function(){
					
				chgPoint = Number(chgPoint) + Number(uncomma($(this).text()));
					
			}); 
			
			//콤마?? TODO
			chgPoint = common.string.setComma(chgPoint);
			
			$(".chgPoint").text(chgPoint);
		}
		
		
		
		$(".swapPoint .layer_pop").hide();
		
		layerAct.close('swapPoint')
	}
	
	function fnPointSwap(){
		shinhanresult = chkShinhanFan();
// 		if(shinhanresult == 'Y' || shinhanresult == 'F'){
// 			goSwap();
// 		} else if(shinhanresult == 'N') {
// 			$(".pop_default").show();
// 			click_lock = true;
// 		} else {
// 			alert("네트워크 오류가 발생하였습니다.");
// 			click_lock = true;
// 		} 
	}
	
	function fngoComfirm(cardNames, resultPoints, resultResults, pointOver){
		$("#resultCardNames").val(cardNames);
		$("#resultPoints").val(resultPoints);
		$("#resultResults").val(resultResults);
		$("#pointOvers").val(pointOver);
		
		$("#form").submit();
	}
	
	function goDown(obj){
		
		/**os type 체크**/
    	var userAgent = navigator.userAgent.toLowerCase();
    	
    	if ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1)	|| (userAgent.search("ipad") > -1)){
    		
    		//신한Fan다운로드 이동
    		if(obj == 'shinhanFan'){
    			window.location = "http://itunes.apple.com/kr/app/sinhan-mobilegyeolje/id572462317?mt=8";
    			
    		}
    		//kb다운로드 이동
    		if(obj == 'kb'){
    			window.location="KTolleh00114://gobrowser?https://m.liivmate.com/appDw";
    			
    		}
    	} else {
    		if(obj == 'shinhanFan'){
    			window.location="KTolleh00114://gobrowser?market://details?id=com.shcard.smartpay";
    		}
    		if(obj == 'kb'){
    			window.location="KTolleh00114://gobrowser?https://m.liivmate.com/appDw";
    		}
    	}	
		
	
	}
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function callOption() {
		/**os type 체크**/
	    	var userAgent = navigator.userAgent.toLowerCase();
			
	    	if ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1)	|| (userAgent.search("ipad") > -1)) {
	    		window.location="jscall://callOption";
	    		// TODO
	    		/* alert('잠금설정 화면으로 이동\nwindow.ClipPoint.callOption()'); */
	    		// 180119 jyi 추가
	    		var _text = "잠금설정 화면으로 이동<br>window.ClipPoint.callOption()";
			$('#notice_word').empty();
			$('#notice_word').append(_text);
			$('#popup_type').val('default');
			$('#alert_type').hide();
			$('#alert_popup').show();
			// //180119 jyi 추가
	    	}
	    	else {
	    		window.ClipPoint.callOption();
	    		// TODO 
	    		/* alert('잠금설정 화면으로 이동\nwindow.ClipPoint.callOption()'); */
	    		// 180119 jyi 추가
	    		var _text = "잠금설정 화면으로 이동<br>window.ClipPoint.callOption()";
			$('#notice_word').empty();
			$('#notice_word').append(_text);
			$('#popup_type').val('default');
			$('#alert_type').hide();
			$('#alert_popup').show();
			// //180119 jyi 추가
	    	}
	}
	
	function sendMidas(_data){
		console.log(_data);
		$.ajax({
			type: "POST",
		    url: "${pageContext.request.contextPath}/pointmain/sendMidas.do",
		    data: _data,
		    type:"post", 
		    dataType	: "json",
		    success: function (resCode) {
		    		console.log(resCode);
		    },
		    error: function(e){
		    		console.log(e);
		    }
		});
	}

</script>
</body>
</html>